#ifndef __EL_ALVO__
#define __EL_ALVO__

#include "cfg.h"

// structures
typedef struct elementstruct{
  int id;
  double x1;
  double y1;
  double x2;
  double y2;
  double x3;
  double y3;
  double x4;
  double y4;
  double bx;
  double by;
} element_t;

// structures
typedef struct alvostruct{
  double x;
  double y;
  double bx;
  double by;
} alvo_t;

//functions

int is_alvo_in_element(alvo_t a, element_t e);
int is_element_in_elipse(element_t e, cfg_t cfg);
int return_elements_in_elipse(element_t **ret,
			      element_t *e,
			      cfg_t cfg,
			      int nel);
int element_calc_baricentro(element_t e,
			    double *x,
			    double *y);
#endif
