#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "pso.h"
#include "cfg.h"
#include "el_alvo.h"

/******************************************************
 *
 *
 * internal definitiions
 *
 *
******************************************************/

#define MAX_NUM_ELEMENTS 4096


/******************************************************
 *
 *
 * internal functions
 *
 *
******************************************************/




/******************************************************
 *
 *
 * external functions
 *
 *
******************************************************/
int read_csv_line_from_file(FILE *fp, double *ret) {
  char *values;
  char *line;
  size_t buffer_size = 256;
  int i;
  
  line = malloc(buffer_size * sizeof(char));

  if (getline(&line, &buffer_size, fp) < 0 ) {
    // end of file
    return 0;
  }

  i = 0;
  values = strtok(line, ",");
  while(values != NULL){
    i++;
    ret = realloc(ret, i * sizeof(double));
    if (ret == NULL) {
      printf("Error allocating double.\n");
      return -1;
    }
    ret[i-1] = atof(values);
    printf("%f\n", ret[i-1]);
    values = strtok(NULL, ",");
  }
  free(line);
  return i;
}

int read_csv_alvo(char *path, alvo_t **a) {
  FILE *fp;
  char *values;
  char *line;
  size_t buffer_size = 256;
  int i, k;
  alvo_t al[MAX_NUM_ELEMENTS];
  
  fp = fopen(path, "r");
  if (fp == NULL) {
    printf("Wrong path or problem to open file, exiting...\n");
    return -1;
  }
  
  line = malloc(buffer_size * sizeof(char));
  i = 0;
  k = 0;
  while(getline(&line, &buffer_size, fp) > 0 && i < MAX_NUM_ELEMENTS) {
    values = strtok(line, ",");
    while(values != NULL){
      k++;
      switch(k) {
      case 1:
	i++;
	al[i-1].x = atof(values);
	break;
      case 2:
	al[i-1].y = atof(values);
	break;
      case 3:
	al[i-1].bx = atof(values);
	break;
      case 4:
	al[i-1].by = atof(values);
	k = 0;
	break;
      default:
	printf("Logic Error.\nExiting...\n");
	return -1;
      }
      values = strtok(NULL, ",");
    }
  }

  if (i >= MAX_NUM_ELEMENTS) {
    printf("The file is to big to be read this way... try increase MAX_NUM_ELEMENTS.\n");
  }
  *a = realloc(*a, i * sizeof(alvo_t));
  memcpy(*a, &al[0], i * sizeof(alvo_t));
  
  free(line);
  fclose(fp);
  return i;
}

int read_csv_out(char *path, element_t **e) {
  FILE *fp;
  char *values;
  char *line;
  size_t buffer_size = 256;
  int i, k, j;
  int val1;
  double val2, val3;
  element_t eo[MAX_NUM_ELEMENTS];
  
  fp = fopen(path, "r");
  if (fp == NULL) {
    printf("Wrong path or problem to open file, exiting...\n");
    return -1;
  }
  
  line = malloc(buffer_size * sizeof(char));
  i = 0;
  k = 0;
  j = 0;
  while(getline(&line, &buffer_size, fp) > 0 && i < MAX_NUM_ELEMENTS) {
    j++;
    values = strtok(line, ",");
    while(values != NULL){
      k++;
      switch(k) {
      case 1:
	val1 = atoi(values);	
	break;
      case 2:
	val2 = atof(values);
	break;
      case 3:
	val3 = atof(values);
	k = 0;
	break;
      default:
	printf("Logic Error.\nExiting...\n");
	return -1;
      }
      values = strtok(NULL, ",");
    }
    // line reading finished
    switch(j) {
    case 1:
      i++;
      eo[i-1].id = val1;
      eo[i-1].x1 = val2;
      eo[i-1].y1 = val3;
      break;
    case 2:
      eo[i-1].x2 = val2;
      eo[i-1].y2 = val3;
      break;
    case 3:
      eo[i-1].x3 = val2;
      eo[i-1].y3 = val3;
      break;
    case 4:
      eo[i-1].x4 = val2;
      eo[i-1].y4 = val3;
      break;
    case 5:
      eo[i-1].bx = val2;
      eo[i-1].by = val3;
      j = 0;
      break;
    default:
      printf("Logic Error.\nExiting...\n");
      return -1;
    }
  }

  if (i >= MAX_NUM_ELEMENTS) {
    printf("The file is to big to be read this way... try increase MAX_NUM_ELEMENTS.\n");
  }
  *e = realloc(*e, i * sizeof(element_t));
  memcpy(*e, &eo[0], i * sizeof(element_t));
  
  free(line);
  fclose(fp);
  return i;
}
