// includes
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "PSOmag.h"

// macro/defines

// internal functions
static int cfg_cpy_eval_struct(cfg_t *self, eval_cfg_t *eval) {
  memcpy(&self->eval, eval, sizeof(eval_cfg_t));
  return 0;
}

static int cfg_cpy_scad_struct(cfg_t *self, scad_cfg_t *scad) {
  memcpy(&self->scad, scad, sizeof(scad_cfg_t));
  return 0;
}

static int cfg_read_file(cfg_t *self, char *file, scad_cfg_t *scad, eval_cfg_t *eval) {
  FILE *cfg;
  int i;
  int equal = 0;
  char *line;
  char endptr;
  char compare[256];
  char value[256];
  size_t lenght, read;

  line = (char *)malloc(512 * sizeof(char));
  
  cfg = fopen(file, "r");
  if (cfg == NULL) {
    printf("Error finding file\n");
    return -1;
  }

  printf("configuration of this simulation:\n\n");
  while((read = getline(&line, &lenght, cfg)) != -1) {
    if (line[0] == '#') {
      continue;
    }
    
    for (i = 0; i < strlen(line) - 1; i++) {
      if (line[i] == '=') {
	equal = i;
      }
      if (equal > 0){
	if (equal == i) {
	  compare[i] = '\0';
	  continue;
	}
	if (line[i] == ' ' || line[i] == '\0') {
	  value[i - equal - 1] = '\0';
	  break;
	}
	value[i - equal -1] = line[i];
	value[i - equal] = '\0';
      } else {
	compare[i] = line[i];
      }
    }
    
    if (strcmp(&compare[0], "simulation_name") == 0) {
      self->sim_name = strdup(value);
      if (self->sim_name == NULL){
	printf("Mem alloc error\n");
	return -1;
      }

      printf("%s=%s\n", compare, self->sim_name);
    } else if (strcmp(&compare[0], "simulation_main_path") == 0) {
      self->sim_path = strdup(value);
      if (self->sim_path == NULL){
	printf("Mem alloc error\n");
	return -1;
      }
      printf("%s=%s\n", compare, self->sim_path);
    } else if (strcmp(&compare[0], "efcs_input_file") == 0) {
      self->input_efcs = strdup(value);
      if (self->input_efcs == NULL){
	printf("Mem alloc error\n");
	return -1;
      }
      
      printf("%s=%s\n", compare, self->input_efcs);
    } else if (strcmp(&compare[0], "efcsouts_input_file") == 0) {
      self->input_efcsouts = strdup(value);
      if (self->input_efcsouts == NULL){
	printf("Mem alloc error\n");
	return -1;
      }
      printf("%s=%s\n", compare, self->input_efcsouts);
    } else if (strcmp(&compare[0], "alvo_name") == 0) {
      self->alvo_path = strdup(value);
      if (self->alvo_path == NULL){
	printf("Mem alloc error\n");
	return -1;
      }
      printf("%s=%s\n", compare, self->alvo_path);
    } else if (strcmp(&compare[0], "max_iterations") == 0) {
      sscanf(value, "%d", &self->maxit);
      printf("%s=%d\n", compare, self->maxit);
    } else if (strcmp(&compare[0], "number_of_particles") == 0) {
      sscanf(value,"%d",&self->n);
      printf("%s=%d\n", compare, self->n);
    } else if (strcmp(&compare[0], "number_of_variables") == 0) {
      sscanf(value, "%d", &self->n_var);
      printf("%s=%d\n", compare, self->n_var);
    } else if (strcmp(&compare[0], "min_var_value") == 0) {
      sscanf(value, "%lf", &self->xmin);
      printf("%s=%f\n", compare, self->xmin);
    } else if (strcmp(&compare[0], "max_var_value") == 0) {
      sscanf(value, "%lf", &self->xmax);
      printf("%s=%f\n", compare, self->xmax);
    } else if (strcmp(&compare[0], "inertial_weight") == 0) {
      sscanf(value, "%lf", &self->w);
      printf("%s=%f\n", compare, self->w);
    } else if (strcmp(&compare[0], "Bx") == 0) {
      sscanf(value, "%lf", &self->bmax);
      printf("%s=%f\n", compare, self->bmax);
    } else if (strcmp(&compare[0], "ima_backlash") == 0) {
      sscanf(value, "%lf", &scad->backlash);
      printf("%s=%f\n", compare, scad->backlash);
    } else if (strcmp(&compare[0], "ima_width") == 0) {
      sscanf(value, "%lf", &scad->real_ima_size);
      printf("%s=%f\n", compare, scad->real_ima_size);
    } else if (strcmp(&compare[0], "thickness_3D") == 0) {
      sscanf(value, "%lf", &scad->thickness);
      printf("%s=%f\n", compare, scad->thickness);
    } else if (strcmp(&compare[0], "eval_circle_radius") == 0) {
      sscanf(value, "%lf", &scad->real_inner_r);
      printf("%s=%f\n", compare, scad->real_inner_r);
    } else if (strcmp(&compare[0], "central_x1") == 0) {
      sscanf(value, "%lf", &eval->xf1);
      printf("%s=%f\n", compare, eval->xf1);
    } else if (strcmp(&compare[0], "central_y1") == 0) {
      sscanf(value, "%lf", &eval->yf1);
      printf("%s=%f\n", compare, eval->yf1);
    } else if (strcmp(&compare[0], "central_x2") == 0) {
      sscanf(value, "%lf", &eval->xf2);
      printf("%s=%f\n", compare, eval->xf2);
    } else if (strcmp(&compare[0], "central_y2") == 0) {
      sscanf(value, "%lf", &eval->yf2);
      printf("%s=%f\n", compare, eval->yf2);
    } else if (strcmp(&compare[0], "elipse_a") == 0) {
      sscanf(value, "%lf", &eval->a);
      printf("%s=%f\n", compare, eval->a);
    } else if (strcmp(&compare[0], "elipse_b") == 0) {
      sscanf(value, "%lf", &eval->b);
      printf("%s=%f\n", compare, eval->b);
    } else {
      printf("%s %s\n", compare, value);
    }
    
    // clean buffers
    equal = 0;
    bzero(&compare[0], 50*sizeof(char));
    bzero(&value[0], 50*sizeof(char));
  }
  
  if (cfg_scad_set(self, scad) < 0) {
    printf("Error loading eval structure.\n");
    return -1;
  }

  if (cfg_eval_set(self, eval) < 0) {
    printf("Error loading eval structure.\n");
    return -1;
  }

  free(line);
  fclose(cfg);
  return 0;
}


static int cfg_read_probes(cfg_t *self, char *file) {
  FILE *cfg;
  int i, j;
  int equal = 0;
  char *line;
  char endptr;
  char compare[256];
  char value[256];
  size_t lenght, read;
  double probex[100], probey[100];

  self->n_probes = 0;
  
  line = (char *)malloc(512 * sizeof(char));
  
  cfg = fopen(file, "r");
  if (cfg == NULL) {
    printf("Error finding file\n");
    return -1;
  }

  printf("Probes of this simulation:\n\n");
  while((read = getline(&line, &lenght, cfg)) != -1) {
    if (line[0] == '#') {
      continue;
    }
    
    for (i = 0; i < strlen(line) - 1; i++) {
      if (line[i] == '=') {
	equal = i;
      }
      if (equal > 0){
	if (equal == i) {
	  compare[i] = '\0';
	  continue;
	}
	if (line[i] == ' ' || line[i] == '\0') {
	  value[i - equal - 1] = '\0';
	  break;
	}
	value[i - equal -1] = line[i];
	value[i - equal] = '\0';
      } else {
	compare[i] = line[i];
      }
    }
    
    if (strncmp(&compare[0], "probex", 6) == 0) {
      sscanf(value, "%lf", &probex[self->n_probes]);
      //printf("%s=%f\n", compare, probex[self->n_probes]);
    } else if (strncmp(&compare[0], "probey", 6) == 0) {
      sscanf(value, "%lf", &probey[self->n_probes]);
      printf("%s=%f\n", compare, probey[self->n_probes]);
      self->n_probes++;
    } else {
      //printf("%s %s\n", compare, value);
    }
    
    // clean buffers
    equal = 0;
    bzero(&compare[0], 50*sizeof(char));
    bzero(&value[0], 50*sizeof(char));
  }

  self->probex = (double *)malloc(self->n_probes * sizeof(double));
  if (self->probex == NULL) {
    printf("Memory allocation error probex\n");
  }
  memcpy(self->probex, &probex[0], self->n_probes * sizeof(double));
  
  self->probey = (double *)malloc(self->n_probes * sizeof(double));
  if (self->probey == NULL) {
    printf("Memory allocation error probex\n");
  }
  memcpy(self->probey, &probey[0], self->n_probes * sizeof(double));

  for (i = 0; i < self->n_probes; i++) {
    printf("probe %d = (%f, %f)\n", i, self->probex[i], self->probey[i]);
  }
  free(line);
  fclose(cfg);
  return 0;
}

// external functions
int cfg_load(cfg_t *self, char *cfg_file) {
  char compare[256]="DEBUG";
  eval_cfg_t eval;
  scad_cfg_t scad;
  if (cfg_read_file(self, cfg_file, &scad, &eval) < 0) {
    printf("error reading file");
    return -1;
  }

  if (cfg_read_probes(self, cfg_file) < 0) {
    printf("error reading probes\n");
    return -1;
  }
  
  return 0;
}

int cfg_eval_set(cfg_t *self, eval_cfg_t *eval) {
  if (eval != NULL) {
    if (cfg_cpy_eval_struct(self, eval) < 0) {
      return -1;
    }
  }
  
  return 0;
}

int cfg_scad_set(cfg_t *self, scad_cfg_t  *scad){
  if (scad != NULL) {
    if (cfg_cpy_scad_struct(self, scad) < 0) {
      return -1;
    }
  }
  return 0;
}
