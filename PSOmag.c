#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <math.h>
#include "PSOmag.h"
#include "evaluate.h"
#include "write.h"
#include "pso.h"
#include <time.h>

#define PARALLELIZATION 1
#define SCAD_FILEPATH "imas.scad"
#define MAIN_SCAD_FILEPATH "main.scad"

static void usage() {
  printf("Usage: <path_to_cfg_file.cfg>\n");
}

pthread_mutex_t lock;

void *PSO_evaluation_function(void *thread_args){
  thread_args_t *args = (thread_args_t *)thread_args;
  PSOmag_t *self = args->main_struct;
  int i = args->i;
  double cost;
  char str_dst[100];
  char str_src[100];
  
  #ifdef PARALLELIZATION
  pso_velocity_update(&self->p[i], self->Pbest[i], self->Gbest, self->cfg.n_var);
  pso_position_update(&self->p[i], self->cfg.n_var);
  
  cost = eval_cost_function(&self->p[i], &self->cfg, i);
  // search for minimization
  if (cost < self->costPbest[i]) {
    self->costPbest[i] = cost;
    sprintf(&str_dst[0],"Pbest%03d", i);
    sprintf(&str_src[0],"%03d", i);
    if (pso_particle_copy(&self->Pbest[i],
			  &self->p[i],
			  self->cfg.n_var,
			  str_dst,
			  str_src) < 0) {
      return NULL;
    }
  }
  #endif
  pthread_exit(NULL);
  return NULL;
}

int main(int argc, char *argv[]) {
  pthread_t *particle_thread;
  pthread_attr_t attr;
  PSOmag_t self;
  thread_args_t *pso_args;
  int t = 0;
  int i, j;
  double cost = INFINITY;
  char str_dst[100];
  char str_src[100];
  char cfg_file[256];
  time_t tini, tfinal;
  char cmd[256];

  
  switch(argc){
  case 1:
    usage();
    return -1;
    break;
  case 2:
    strcpy(cfg_file, argv[1]);
    break;
  default:
    usage();
    return -1;
    break;
  }

  tini = time(NULL);
  if (cfg_load(&self.cfg, cfg_file) < 0) {
    return -1;
  }

  particle_thread = malloc(self.cfg.n * sizeof(pthread_t));
  pso_args = malloc(self.cfg.n * sizeof(thread_args_t));
  pthread_mutex_init(&lock, NULL);
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  
  pso_initialize_particles(&self);
  
  while (t < self.cfg.maxit) {
    #ifdef PARALLELIZATION
    for (i = 0; i < self.cfg.n; i++) {
      pso_args[i].i = i;
      pso_args[i].main_struct = &self;
      if (pthread_create(&particle_thread[i],
			 &attr,
			 PSO_evaluation_function,
			 &pso_args[i]) < 0) {
	printf("Error creating thread.\n");
      }
    }
    #endif
    for (i = 0; i < self.cfg.n; i++) {
      #ifndef PARALLELIZATION
      pso_velocity_update(&self.p[i], self.Pbest[i], self.Gbest, self.cfg.n_var);
      pso_position_update(&self.p[i], self.cfg.n_var);
      
      cost = eval_cost_function(&self.p[i], &self.cfg, i);
      // search for minimization
      if (cost < self.costPbest[i]) {
	self.costPbest[i] = cost;
	sprintf(&str_dst[0],"Pbest%03d", i);
	sprintf(&str_src[0],"%03d", i);
	if (pso_particle_copy(&self.Pbest[i],
			  &self.p[i],
			  self.cfg.n_var,
			  str_dst,
			  str_src) < 0) {
	  return -1;
	}
      }
      #endif

      //it is need to ensure that the correspondant thread is
      //up to be used.
      #ifdef PARALLELIZATION
      if (pthread_join(particle_thread[i], NULL) < 0) {
	printf("Error Joining thread.\n");
      }
    }
    for (i = 0; i < self.cfg.n; i++) {
      #endif
      if (self.costPbest[i] < self.costGbest) {
	self.costGbest = self.costPbest[i];
	sprintf(&str_dst[0],"Gbest");
	sprintf(&str_src[0],"Pbest%03d", i);

	if (pso_particle_copy(&self.Gbest,
			      &self.Pbest[i],
			      self.cfg.n_var,
			      str_dst,
			      str_src) < 0) {
	  return -1;
	}
	
	// Only for debug purpouses
	printf("iteration=%d new Gbest cost=%0.4g\n",t, self.costGbest);
	for (j = 0; j < self.cfg.n_var; j++) {
	  printf("GbestX(%d) = %f\n", j, self.Gbest.x[j]);
	}
      }
    }
    wrt_convergence(&self, t);
    // check for stop criteria

    
    // Only for debug purpouses
    printf("iteration=%d Gbest cost=%0.4g\n",t, self.costGbest);
    t++;
  }
  
  wrt_results(&self);
  wrt_scad_file(&self, SCAD_FILEPATH);
  wrt_cpy_to_path("results.csv", self.cfg.sim_path);
  wrt_cpy_to_path("convergence.csv", self.cfg.sim_path);
  wrt_cpy_to_path(MAIN_SCAD_FILEPATH, self.cfg.sim_path);
  wrt_cpy_to_path(SCAD_FILEPATH, self.cfg.sim_path);
  sprintf(&cmd[0],"./cp_all.sh %s", self.cfg.sim_path);

  if (system(cmd) < 0) {
    printf("error copying\n");
  }
  
  PSO_free_all(&self);
  free(particle_thread);
  free(pso_args);
  tfinal = time(NULL);
  printf("\n\nThis program run in %f seconds!\n\n", difftime(tfinal, tini));
  pthread_exit(NULL);
  return 0;
}

int PSO_free_all(PSOmag_t *self) {
  int i;
  free(self->cfg.sim_name);
  free(self->cfg.input_efcs);
  free(self->cfg.input_efcsouts);
  for (i = 0; i < self->cfg.n_var; i++) {
    free(self->p[i].r1);
    free(self->Pbest[i].r1);
    free(self->p[i].r2);
    free(self->Pbest[i].r2);
    free(self->p[i].c1);
    free(self->Pbest[i].c1);
    free(self->p[i].c2);
    free(self->Pbest[i].c2);
    free(self->p[i].x);
    free(self->Pbest[i].x);
    free(self->p[i].v);
    free(self->Pbest[i].v);
    free(self->p[i].w);
    free(self->Pbest[i].w);
  }
  free(self->p);
  free(self->Pbest);
  free(self->costPbest);
  free(self->Gbest.r1);
  free(self->Gbest.r2);
  free(self->Gbest.c1);
  free(self->Gbest.c2);
  free(self->Gbest.x);
  free(self->Gbest.v);
  free(self->Gbest.w);
  return 0;
}
