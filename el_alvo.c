#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "cfg.h"
#include "el_alvo.h"

/******************************************************
 *
 *
 * internal definitiions
 *
 *
******************************************************/

#define MAX_ELEM_IN 4096


/******************************************************
 *
 *
 * internal functions
 *
 *
******************************************************/

static void element_copy(element_t *dst, element_t src) {
    memcpy(dst, &src, sizeof(element_t));
    return;
}

static int is_point_in_elipse(double x1,
			      double y1,
			     double Xf1,
			     double Yf1,
			     double Xf2,
			     double Yf2,
			     double a,
			     double b) {
  double Xc, Yc;
  double Xmin, Xmax, Ymin, Ymax;
  double x, y;
  double costheta, sintheta, theta;
  double aux;
  
  Xc = (Xf1 + Xf2) / 2.0;
  Yc = (Yf1 + Yf2) / 2.0;
  if (Xf1 - Xf2 == 0.0) {
    theta = M_PI/2.0;
  }
  else {
    theta = atan((Yf2 - Yf1)/(Xf1 - Xf2));
  }
  sintheta = sin(theta);
  costheta = cos(theta);
  x1 = x1 - Xc;
  y1 = y1 - Yc;
  x = x1 * costheta + y1 * (sintheta);
  y = x1 * (-sintheta) + y1 * costheta;

  Xmin = - a;
  Xmax = + a;
  if (x < Xmin || x > Xmax) {
    return 0;
  }
  
  aux = b*b - ((b * b) * (x * x)) / (a * a);
  if (aux < 0 ) {
    return 0;
  }

  Ymin = -sqrt(aux);
  Ymax = sqrt(aux);
  if (y < Ymin || y > Ymax) {
    return 0;
  }
  
  return 1;
}

static double min_x_element(element_t e) {
  double min = INFINITY;
  
  if (e.x1 < min) {
    min = e.x1;
  }
  if (e.x2 < min) {
    min = e.x2;
  }
  if (e.x3 < min) {
    min = e.x3;
  }
  
  if ((e.x4 != 0) || (e.y4 != 0)) {
    if (e.x4 < min) {
      min = e.x4;
    }
  }
  return min;
}

static double max_x_element(element_t e) {
  double max = -INFINITY;
  
  if (e.x1 > max) {
    max = e.x1;
  }
  if (e.x2 > max) {
    max = e.x2;
  }
  if (e.x3 > max) {
    max = e.x3;
  }
  
  if ((e.x4 != 0) || (e.y4 != 0)) {
    if (e.x4 > max) {
      max = e.x4;
    }
  }
  return max;
}

static double min_y_element(element_t e) {
  double min = INFINITY;
  
  if (e.y1 < min) {
    min = e.y1;
  }
  if (e.y2 < min) {
    min = e.y2;
  }
  if (e.y3 < min) {
    min = e.y3;
  }
  
  if ((e.x4 != 0) || (e.y4 != 0)) {
    if (e.y4 < min) {
      min = e.y4;
    }
  }
  return min;
}

static double max_y_element(element_t e) {
  double max = -INFINITY;
  
  if (e.y1 > max) {
    max = e.y1;
  }
  if (e.y2 > max) {
    max = e.y2;
  }
  if (e.y3 > max) {
    max = e.y3;
  }
  
  if ((e.x4 != 0) || (e.y4 != 0)) {
    if (e.y4 > max) {
      max = e.y4;
    }
  }
  return max;
}


/******************************************************
 *
 *
 * external functions
 *
 *
******************************************************/
int is_alvo_in_element(alvo_t a, element_t e) {

  if (a.x >= min_x_element(e) &&
      a.x <= max_x_element(e) &&
      a.y >= min_y_element(e) &&
      a.y <= max_y_element(e)) {
    return 1;
  }
    
  return 0;
}

int is_element_in_elipse(element_t e, cfg_t cfg) {
  double x,y;
  
  if (element_calc_baricentro(e, &x, &y) < 0) {
    return -1;
  }
  
  return is_point_in_elipse( x, y,
			      cfg.eval.xf1,
			      cfg.eval.yf1,
			      cfg.eval.xf2,
			      cfg.eval.yf2,
			      cfg.eval.a,
			      cfg.eval.b);
}

int return_elements_in_elipse(element_t **ret, element_t *e, cfg_t cfg, int nel) {
  int i, k;
  element_t cp[MAX_ELEM_IN];

  k = 0;
  for (i = 0; i < nel; i++) {
    if (k > MAX_ELEM_IN) {
      printf("Exceded max of elements.\n");
      return -1;
    }
    if (is_element_in_elipse(e[i], cfg)) {
      element_copy(&cp[k], e[i]);
      k++;
    }
  }

  *ret = realloc(*ret, k * sizeof(element_t));
  memcpy(*ret, &cp[0], k * sizeof(element_t));
  return k;
}

int element_calc_baricentro(element_t e,
			      double *x,
			      double *y) {

  if (e.x4 == 0 && e.y4 == 0) {
    *x = (e.x1 + e.x2 + e.x3) / 3.0;
    *y = (e.y1 + e.y2 + e.y3) / 3.0;
  }
  else {
    *x = (e.x1 + e.x2 + e.x3 + e.x4) / 4.0;
    *y = (e.y1 + e.y2 + e.y3 + e.y4) / 4.0;
  }

  return 0;
}
