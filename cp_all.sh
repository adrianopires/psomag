#!/bin/sh

if [ -d $1 ]; then
    echo "coppyng files..."
else
    mkdir $1
    echo "coppyng files..."
fi

cp *.csv $1;
cp *.scad $1/3D;
cp -R Gbest $1;
mkdir $1/particles;
cp -R Pbest* $1/particles;
cp *.pre $1
cp *.elf $1
rm -rf *.pre *.elf
