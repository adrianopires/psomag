CCOMPILER=gcc
CFLAGS= -Wall
LFLAGS= -lm -lpthread
OBJECTS=cfg.o evaluate.o pso.o write.o read.o efcad_aux.o el_alvo.o

all: ${OBJECTS}
	${CCOMPILER} ${CFLAGS} PSOmag.c ${OBJECTS} -o pso

csrc:
	rm -rf *~ \#* *# *.o *.a *.csv *.input imas.scad

clean:
	rm -rf *~ \#* *# *.o *.a *.csv *.input pso [0-9]* Pbest[0-9]* Gbest scr*.sh

result:
	rm -rf *~ \#* *# *.o *.a *.csv *.input pso [0-9]* scr*.sh

clean_all: clean result csrc
	rm -rf *.pre *.elf *.dat
