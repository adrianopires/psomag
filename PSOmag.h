#include "cfg.h"
#include "pso.h"

#ifndef __PSOMAG__
#define __PSOMAG__

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

typedef struct PSOmag{
  cfg_t cfg;
  pso_particle_t *p; // n particles dimension
  double *costPbest;
  double costGbest;
  pso_particle_t *Pbest;
  pso_particle_t Gbest;
}PSOmag_t;

typedef struct PSOthread_args{
  PSOmag_t *main_struct;
  int i;
}thread_args_t;


int PSO_free_all(PSOmag_t *self);
#endif
