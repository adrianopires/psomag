#ifndef __EVALUATE__
#define __EVALUATE__

#include "pso.h"
#include "cfg.h"

//functions
double eval_cost_function(pso_particle_t *p, cfg_t *cfg, int particle);

#endif
