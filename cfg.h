#ifndef __CFG__
#define __CFG__

typedef struct evalcfg {
  double xf1, xf2;
  double yf1, yf2;
  double a;
  double b;
} eval_cfg_t;

typedef struct scad_cfg {
  double backlash;
  double real_ima_size;
  double thickness;
  double real_inner_r;
} scad_cfg_t;

// structures
typedef struct cfg {
  char *sim_name;
  char *sim_path;
  char *input_efcs;
  char *input_efcsouts;
  char *alvo_path;
  int maxit;
  int n;
  int n_var;
  double xmin;
  double xmax;
  double bmax; // unit tesla
  double w;
  eval_cfg_t eval;
  scad_cfg_t scad;
  int n_probes;
  double *probex;
  double *probey;
} cfg_t;


// functions

int cfg_load(cfg_t *self, char *cfg_file);

int cfg_eval_set(cfg_t *self, eval_cfg_t *eval);

int cfg_scad_set(cfg_t *self, scad_cfg_t *scad);

#endif
