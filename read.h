#ifndef __READ__
#define __READ__

#include "cfg.h"
#include "pso.h"
#include "el_alvo.h"

int read_csv_line_from_file(FILE *fp, double *ret);

int read_csv_alvo(char *path, alvo_t **a);
int read_csv_out(char *path, element_t  **e);

#endif
