% funcao para gerar o arquivo efmat
function ret = gera_efmat(filename, n_material, Bx, By, path_auxiliar)
  
   source([path_auxiliar "getScientificFortran.m"]);
   fp = fopen(filename, "w");
   permeabilidade_ar = 1;
   limite = 30;
   % escreve o material ar
   fprintf(fp,"  1 ar                  \n");
   fprintf(fp,"LIN%05.4E",permeabilidade_ar);
   for i=1:8
     fprintf(fp,"%05.4E",0);
   end
   fprintf(fp,"\n");
   for k=1:10
     for i=1:10
       fprintf(fp,"%05.4E",0);
     end
     fprintf(fp,"\n");
   end
   
   for i=2:n_material+1
     nome = sprintf("ima%d",i);
     if i < 10
       fprintf(fp,"  %d %s                \n",i,nome);
     else
       fprintf(fp," %d %s               \n",i,nome);
     end
     fprintf(fp,"IMA");
     fprintf(fp,"%05.4E",1); % permeabilidade relativa
     
     [baseBx expoenteBx sinalBx] = getScientificFortran(Bx(i-1));
     wbaseBx = sprintf("%5.4f",baseBx);
     if sinalBx == 1
       wbaseBx = strrep(wbaseBx,"0.","-.");
     end
     
     [baseBy expoenteBy sinalBy] = getScientificFortran(By(i-1));
     wbaseBy = sprintf("%5.4f",baseBy);
     if sinalBy == 1
       wbaseBy = strrep(wbaseBy,"0.","-.");
     end
     
     fprintf(fp,"%sE%+0.2d",wbaseBx,expoenteBx); % Bx
     fprintf(fp,"%sE%+0.2d",wbaseBy,expoenteBy); % By
     for p=3:8
       fprintf(fp,"%05.4E",0);
     end
     fprintf(fp,"\n");
     for k=1:10
       for n=1:10
	 fprintf(fp,"%05.4E",0);
       end
       fprintf(fp,"\n");
     end
   end

   
   for i=n_material+2:limite
   % escreve o material null
   if i < 10
     fprintf(fp,"  %d                     \n",i);
   else
      fprintf(fp," %d                     \n",i);
    end
   fprintf(fp,"NAO%05.4E",0);
   for i=1:8
     fprintf(fp,"%05.4E",0);
   end
   fprintf(fp,"\n");
   for k=1:10
     for i=1:10
       fprintf(fp,"%05.4E",0);
     end
     fprintf(fp,"\n");
   end
   end
   fclose(fp);
endfunction
