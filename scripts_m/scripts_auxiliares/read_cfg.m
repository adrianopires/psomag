function [vreturn] = read_cfg(path)

vreturn = cell(20,1);
cfg = fopen(path, "r");

while((str = fgetl(cfg)) != -1)
  if (str(1) == "#")
    continue;
  end
  
  split = strsplit(str,"=");
  compare = char(split(1,1));
  value = char(split(1,2));
  
  if (strcmp(compare,"simulation_name") == 1) 
    vreturn(1) = sscanf(value,"%s");
  elseif (strcmp(compare,"simulation_main_path") == 1) 
    vreturn(2) = sscanf(value,"%s");
  elseif (strcmp(compare,"simulation_exec_path") == 1) 
    vreturn(3) = sscanf(value,"%s");
  elseif (strcmp(compare,"efcs_input_file") == 1) 
    vreturn(4) = sscanf(value,"%s");
  elseif (strcmp(compare,"efcsouts_input_file") == 1) 
    vreturn(5) = sscanf(value,"%s");
  elseif (strcmp(compare,"result_name") == 1) 
    vreturn(6) = sscanf(value,"%s");
  elseif (strcmp(compare,"alvo_name") == 1) 
    vreturn(7) = sscanf(value,"%s");
  elseif (strcmp(compare,"max_iteration") == 1) 
    vreturn(8) = sscanf(value,"%d");
  elseif (strcmp(compare,"number_of_particles") == 1) 
    vreturn(9) = sscanf(value,"%d");
  elseif (strcmp(compare,"number_of_variables") == 1) 
    vreturn(10) = sscanf(value,"%d");
  elseif (strcmp(compare,"min_var_value") == 1) 
    vreturn(11) = sscanf(value,"%f");
  elseif (strcmp(compare,"max_var_value") == 1) 
    vreturn(12) = sscanf(value,"%f");
  elseif (strcmp(compare,"inertial_weight") == 1) 
    vreturn(13) = sscanf(value,"%f");
  elseif (strcmp(compare,"Bx") == 1) 
    vreturn(14) = sscanf(value,"%f");
  elseif (strcmp(compare,"By") == 1) 
    vreturn(15) = sscanf(value,"%f");
  elseif (strcmp(compare,"ima_backlash") == 1) 
    vreturn(16) = sscanf(value,"%f");
  elseif (strcmp(compare,"ima_width") == 1) 
    vreturn(17) = sscanf(value,"%f");
  elseif (strcmp(compare,"ima_height") == 1) 
    vreturn(18) = sscanf(value,"%f");
  elseif (strcmp(compare,"thickness_3D") == 1) 
    vreturn(19) = sscanf(value,"%f");
  elseif (strcmp(compare,"eval_circle_radius") == 1) 
    vreturn(20) = sscanf(value,"%f");
  elseif (strcmp(compare,"divx") == 1) 
    vreturn(21) = sscanf(value,"%d");
  elseif (strcmp(compare,"divy") == 1) 
    vreturn(22) = sscanf(value,"%d");
  elseif (strcmp(compare,"result_for_alvo_path") == 1) 
    vreturn(23) = sscanf(value,"%s");
  elseif (strcmp(compare,"central_x1") == 1) 
    vreturn(24) = sscanf(value,"%f");
  elseif (strcmp(compare,"central_y1") == 1) 
    vreturn(25) = sscanf(value,"%f");
  elseif (strcmp(compare,"central_x2") == 1) 
    vreturn(26) = sscanf(value,"%f");
  elseif (strcmp(compare,"central_y2") == 1) 
    vreturn(27) = sscanf(value,"%f");
  elseif (strcmp(compare,"elipse_a") == 1) 
    vreturn(28) = sscanf(value,"%f");
  elseif (strcmp(compare,"elipse_b") == 1) 
    vreturn(29) = sscanf(value,"%f");
  else
  end
  
  clear split;
end

fclose(cfg);
return;
endfunction

function path = get_path_cfg()
   path = "../../padrao.cfg";
   
   new_path = input("Please, inform configuration path (enter to default path): ", "s");
  
   if (strcmp(new_path,"") == 1)
     return;
   end
   
   path = new_path;
   return;
   
endfunction

function [n_probes probex_return probey_return] = read_probes(path)

probex_return = 0;
probey_return = 0;
cfg = fopen(path, "r");
i = 1;
while((str = fgetl(cfg)) != -1)
  if (str(1) == "#")
    continue;
  end
  
  split = strsplit(str,"=");
  compare = char(split(1,1));
  value = char(split(1,2));
  
  if (strncmp(compare,"probe", 5) == 1) 
    if (strncmp(compare,"probex", 6) == 1)
      probex_return(i) = sscanf(value,"%f");
    else
      probey_return(i) = sscanf(value,"%f");
      i=i+1;
    end
  else
  end
  
  clear split;
end
n_probes = i - 1;
fclose(cfg);
return;
endfunction
