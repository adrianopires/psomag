function [alvo, Nalvo] = gAlvoConstSquare(Box, Boy, Nx, Ny, xi, yi, xf, yf)
	dx = (xf-xi) / Nx;
	dy = (yf-yi) / Ny;
	k=1;
	for i=1:Nx
		for j=1:Ny
			alvo(k,1) = xi + dx*(i-1);
			alvo(k,2) = yi + dy*(j-1);
			alvo(k,3) = Box;
			alvo(k,4) = Boy;
			k=k+1;
		end
	end
	Nalvo = k-1;
endfunction

function [alvo, Nalvo] = gAlvoConstElip(Box, Boy, Nx, Ny, Xf1, Yf1, Xf2, Yf2, a, b)
	dx = (2*a) / Nx;
	Xc = (Xf1 + Xf2) / 2;
	Yc = (Yf1 + Yf2) / 2;
	if Xf1-Xf2 == 0
		theta = pi/2;
	else
		theta = atan((Yf2 - Yf1)/(Xf1-Xf2));
	end
	k=1;
	xi = - a;
	for i=1:Nx
		xa = xi + dx*(i-1);
		if (b^2 - (b^2) * (xa^2)/(a^2) < 0)
			continue;
		end
		Ymax = sqrt(b^2 - (b^2) * (xa^2)/(a^2));
		yi = (0);
		dy = (2*(Ymax - yi)) / Ny;
		if dy == 0
			continue;
		end
		sintheta = sin(theta);
		costheta = cos(theta);
		for j=1:Ny
			x1 = xa;
			y1 = yi - Ymax + dy*(j-1);
			x = x1 * costheta + y1 * sintheta;
			y = x1 * (-sintheta) + y1 * costheta;
			alvo(k,1) = x + Xc;
			alvo(k,2) = y + Yc;
			alvo(k,3) = Box;
			alvo(k,4) = Boy;
			k=k+1;
		end
	end
	Nalvo = size(alvo,1);
endfunction

function writeAlvo(input_file, alvo)
	fp=fopen(input_file, "w");
	if (fp < 0)
	  printf("%Error! I couldn't open %s\n\n\n", input_file);
	end
	
	for i=1:size(alvo,1)
		fprintf(fp,"%g, %g, %g, %g\n", alvo(i,1), alvo(i,2), alvo(i,3), alvo(i,4));
	end
	fclose(fp);
endfunction