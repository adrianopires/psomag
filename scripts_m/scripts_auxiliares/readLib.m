% script para ler uma malha de EFCAD formato .pre
function [xb, yb]=baricentro(element)
	if (element(7) == 0 &&  element(8) == 0)
		xb = (element(1) + element(3) + element(5)) / 3;
		yb = (element(2) + element(4) + element(6)) / 3;
	else
		xb = (element(1) + element(3) + element(5) + element(7)) / 4;
		yb = (element(2) + element(4) + element(6) + element(8)) / 4;
	end
endfunction

function [elements,Nel]=read_out_all(input_file)

elements = zeros(1,10);
fp = fopen(input_file,"r");
k = 0;
while((line = fgetl(fp)) > 0)
	temp = sscanf(line, "\t%d\t,\t%g\t,\t%g\t");
	elements(temp(1),k + 1) = temp(2);
	elements(temp(1),k + 2) = temp(3);
	if k == 8
		k = 0;
	else
		k=k+2;
	end
end
Nel = temp(1);
fclose(fp);
endfunction

function ret = isInElipse(Xf1, Yf1, Xf2, Yf2, a, b, x1, y1)
	ret = 1;
	Xc = (Xf1 + Xf2) / 2;
	Yc = (Yf1 + Yf2) / 2;
	if Xf1-Xf2 == 0
		theta = pi/2;
	else
		theta = atan((Yf2 - Yf1)/(Xf1-Xf2));
	end
	sintheta = sin(theta);
	costheta = cos(theta);
	x1 = x1 - Xc;
	y1 = y1 - Yc;
	x = x1 * costheta + y1 * sintheta;
	y = x1 * (-sintheta) + y1 * costheta;
	
	Xmin = - a;
	Xmax = + a;
	if (x < Xmin || x > Xmax)
		ret = 0;
		return;
	end

	if ((b^2 - (b^2) * (x^2)/(a^2)) < 0 )
		ret = 0;
		return;
	end

	Ymin = -sqrt(b^2 - (b^2) * (x^2)/(a^2));
	Ymax = sqrt(b^2 - (b^2) * (x^2)/(a^2));
	if (y < Ymin || y > Ymax)
		ret = 0;
		return;
	end
	return;
endfunction

function [elements, Nel] = read_out_elipse(input_file, Xf1, Yf1, Xf2, Yf2, a, b)

[outTotal nTotal] = read_out_all(input_file);
k = 1;
for i=1:nTotal
	[xb yb] = baricentro(outTotal(i,:));
	if (isInElipse(Xf1, Yf1, Xf2, Yf2, a, b, xb, yb) == 1)
		elements(k,:) = outTotal(i,:);
		k=k+1;
	end
end
Nel = k-1;
endfunction

function [elements,Nel]=read_out_square(input_file, xi,yi,xf,yf)

[outTotal nTotal] = read_out_all(input_file);
k = 1;
for i=1:nTotal
	[xb yb] = baricentro(outTotal(i,:));
	if (xb > xi && xb < xf && yb > yi && yb < yf)
		elements(k,:) = outTotal(i,:);
		k=k+1;
	end
end
Nel = k-1;
endfunction

function [alvo,Nalvo]=read_alvo(input_file)

alvo = zeros(1,4);
fp = fopen(input_file,"r");
k = 1;
while((line = fgetl(fp)) > 0)
	oi = sscanf(line, "%g, %g, %g, %g");
	alvo(k, 1) = oi(1);
	alvo(k, 2) = oi(2);
	alvo(k, 3) = oi(3);
	alvo(k, 4) = oi(4);
	k=k+1;
end
Nalvo = k - 1;
fclose(fp);
endfunction



function [alvo, Nalvo] = gAlvoFromPathElipse(input_file, Xf1, Yf1, Xf2, Yf2, a, b)

[outTotal nTotal] = read_out_all(input_file);
k = 1;
for i=1:nTotal
  [xb yb] = baricentro(outTotal(i,:));
  if (isInElipse(Xf1, Yf1, Xf2, Yf2, a, b, xb, yb) == 1)
    alvo(k,1) = xb;
    alvo(k,2) = yb;
    alvo(k,3) = outTotal(i,9);
    alvo(k,4) = outTotal(i,10);
    k=k+1;
  end
end
Nalvo = k-1;

endfunction