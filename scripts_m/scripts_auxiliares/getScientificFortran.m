function [base, expoente, minus] = getScientificFortran(value)
minus = 0;
expoente = 0;
base = 0;

if value < 0
  minus = 1;
  value = -value;
end

if value > 1
  expoente = 0;
end

if value == 0
  return;
end

while value >= 1
  expoente = expoente + 1;
  value=value/10;
end

while value < 0.1
  expoente = expoente - 1;
  value=value*10;
end

base = value;
endfunction
