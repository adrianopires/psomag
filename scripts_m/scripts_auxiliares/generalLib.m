function ret = copy_simulatioAn(simulation, stringtocopy)

      string_cmd1 = sprintf("cp %s.elf %s.elf", simulation, stringtocopy);
      [ret text_return] = system(string_cmd1);
      
      if (ret != 0)
	printf("Erro ao copiar arquivo de simulacao .elf");
	return;
      end
      
      string_cmd1 = sprintf("cp efmat.dat efmat_%s.dat", stringtocopy);
      [ret text_return] = system(string_cmd1);
      
      if (ret != 0)
	printf("Erro ao copiar arquivo efmat.dat de simulacao");
	return;
      end
      
endfunction

function ret = exec_EFCAD(path_efcs, path_efcsout)
    [ret text_return] = system(path_efcs);
    if ( ret != 0 )
      printf("Erro ao executar efcs\n... Parando a Execucao...\n");
      return;
    end
    [ret text_return] = system(path_efcsout);
    if ( ret != 0 )
      printf("Erro ao executar efcsouts\n... Parando a Execucao...\n");
      return;
    end
endfunction

function ret = isAlvoInElement(alvo, element)
	if (element(7) == 0 && element(8) == 0)
		xel = [element(1) element(3) element(5)];
		yel = [element(2) element(4) element(6)];
	else
		xel = [element(1) element(3) element(5) element(7)];
		yel = [element(2) element(4) element(6) element(8)];
	end
	minx = min(xel); maxx = max(xel);
	miny = min(yel); maxy = max(yel);
	if (alvo(1) > minx &&
		alvo(1) < maxx &&
		alvo(2) > miny &&
		alvo(2) < maxy)
		ret = 1;
	else
		ret = 0;
	end
endfunction

function ret = isPointInElement(px, py, element)
	if (element(7) == 0 && element(8) == 0)
		xel = [element(1) element(3) element(5)];
		yel = [element(2) element(4) element(6)];
	else
		xel = [element(1) element(3) element(5) element(7)];
		yel = [element(2) element(4) element(6) element(8)];
	end
	minx = min(xel); maxx = max(xel);
	miny = min(yel); maxy = max(yel);
	if (px > minx &&
		px < maxx &&
		py > miny &&
		py < maxy)
		ret = 1;
	else
		ret = 0;
	end
endfunction

function [bxe, bye, tve] = TVE(alvos, elements)
	bxe = 0; bye = 0; tve = 0;
	Nel = size(elements,1);
	Nalvo = size(alvos,1);
	for (i = 1:Nalvo)
		for (j = 1:Nel)
			if (isAlvoInElement(alvos(i,:), elements(j,:)))
				ex = sqrt( (alvos(i,3) - elements(j,9))^2 );
				ey = sqrt( (alvos(i,4) - elements(j,10))^2 );
				bxe = bxe + ex;
				bye = bye + ey;
				tve = tve + ex + ey;
				break;
			end
		end
	end
endfunction

function write_resumo_simu(tve, tveGbest)
  fp = fopen("resumo_resultados.tmp","w");
  if (fp < 0)
    return;
  end
  
  for i=1:numel(tve)
    fprintf(fp,"Particula %d TVE Pbest = %f\n", i, tve(i));
  end
  fprintf(fp,"TVE GBEST = %f", tveGbest);
  fclose(fp);
  system("cp resumo_resultados.tmp resumo_resultados.txt");
  system("rm resumo_resultados.tmp");
endfunction


function [bx by] = eval_probe(probex,probey, elements)
	bxe = 0; bye = 0; tve = 0;
	Nel = size(elements,1);
	for (j = 1:Nel)
	  if (isPointInElement(probex, probey, elements(j,:)))
	    bx = elements(j,9);
	    by = elements(j,10);
	    break;
	  end
	end
endfunction

function [bx by] = eval_probe_alvo(probex,probey, alvo, limiar)
        N = size(alvo,1);
	min = inf;
	lasti = 0;
	for (i = 1:N)
	  dist = sqrt((alvo(i,1)-probex)^2 + (alvo(i,2)-probey)^2);
	  if (dist <= min)
	    min = dist;
	    lasti = i;
	  end
	  if (dist < limiar)
	    bx = alvo(lasti,3);
	    by = alvo(lasti,4);
	    return;
	  end
	end
	
	
	%printf("Could Not found Probe (%f,%f) for limiar %f", probex, probey, limiar);
	
	bx = alvo(lasti,3);
	by = alvo(lasti,4);
	return;
endfunction