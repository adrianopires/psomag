function ret=gera_pre(filename, unit, p1_ima, p2_ima, p3_ima, p4_ima,
  p1_caixa, p2_caixa, p3_caixa, p4_caixa, ang_ini_ima)
  fp = fopen(filename,"w");
  
  fprintf(fp, "  %d.\r\n",unit);
  counter = 0;
  fprintf(fp, " %d %d 0\r\n", ++counter, ++counter);%p1_caixa->p2_caixa
  fprintf(fp, " %d %d 0\r\n", counter, ++counter);%p2_caixa->p3_caixa
  fprintf(fp, " %d %d 0\r\n", counter, ++counter);%p3_caixa->p4_caixa
  fprintf(fp, " %d %d 0\r\n", counter, counter-3);%p4_caixa->p1_caixa
  for i=1:1
    p1_last=++counter;
    p2_last=++counter;
    p3_last=++counter;
    p4_last=++counter;
    fprintf(fp, " %d %d 0\r\n", p1_last, p2_last);%p1_ima->p2_ima
    fprintf(fp, " %d %d 0\r\n", p2_last, p3_last);%p2_ima->p3_ima
    fprintf(fp, " %d %d 0\r\n", p3_last, p4_last);%p3_ima->p4_ima
    fprintf(fp, " %d %d 0\r\n", p4_last, p1_last);%p4_ima->p1_ima
  end
  for i=2:size(p1_ima)(1)
    p1=++counter;
    p2=++counter;
    p3=++counter;
    p4=++counter;
    % "liga" os imas
    fprintf(fp, " %d %d 0\r\n", p1, p2);%p1_ima->p2_ima
    fprintf(fp, " %d %d 0\r\n", p2, p3);%p2_ima->p3_ima
    fprintf(fp, " %d %d 0\r\n", p3, p4);%p3_ima->p4_ima
    fprintf(fp, " %d %d 0\r\n", p4, p1);%p4_ima->p1_ima
    
    % "liga" os ultimos pontos dos imas para evitar erros de conexao entre pecas
    fprintf(fp, " %d %d 0\r\n", p1, p2_last);
    fprintf(fp, " %d %d 0\r\n", p4, p3_last);
    
    p1_last = p1; p2_last = p2; p3_last = p3; p4_last = p4;
  end
  fprintf(fp, "F\r\n  %.6f  %.6f\r\n", p1_caixa(1), p1_caixa(2));
  fprintf(fp, "  %.6f  %.6f\r\n", p2_caixa(1), p2_caixa(2));
  fprintf(fp, "  %.6f  %.6f\r\n", p3_caixa(1), p3_caixa(2));
  fprintf(fp, "  %.6f  %.6f\r\n", p4_caixa(1), p4_caixa(2));

  for i=1:size(p1_ima)(1)
    fprintf(fp, "  %.6f  %.6f\r\n", p1_ima(i,1), p1_ima(i,2));
    fprintf(fp, "  %.6f  %.6f\r\n", p2_ima(i,1), p2_ima(i,2));
    fprintf(fp, "  %.6f  %.6f\r\n", p3_ima(i,1), p3_ima(i,2));
    fprintf(fp, "  %.6f  %.6f\r\n", p4_ima(i,1), p4_ima(i,2));
  end
  fprintf(fp, "F\r\nF\r\n");
  fclose(fp);
endfunction