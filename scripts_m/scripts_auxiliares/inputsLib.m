function ret=gera_input_efcs(filename, simulation_name)
	fp = fopen(filename,"w");
		fprintf(fp, "%s\n", simulation_name);
		fprintf(fp, "\n"); %mesh plotting?
		fprintf(fp, "\n"); %Cartesian or axissimetrica
		fprintf(fp, "\n"); %Linear or non linear
	fclose(fp);
	ret = 0;
	return;	
endfunction

function ret=gera_input_efcsouts(filename, simulation_name, path_out)
	fp = fopen(filename,"w");
		fprintf(fp, "%s\n", simulation_name);
		fprintf(fp, "\n"); %mesh plotting?
		fprintf(fp, "%s\n", path_out);
	fclose(fp);
	ret = 0;
	return;
endfunction