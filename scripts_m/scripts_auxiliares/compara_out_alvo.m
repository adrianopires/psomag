clear
clc

source("alvoLib.m");
source("readLib.m");
source("generalLib.m");

alvo_name = "alvo.alvo";
out_name = "teste.out";

unit = 1000;
altura_ima = 10; % em mm
largura_ima = 10; % em mm
raio_interno = 30; % em mm
max_alt_larg = max (altura_ima, largura_ima);

altura_caixa = 1.80 * ( max_alt_larg * 2 + raio_interno * 2) 
p_central_x = altura_caixa / 2

largura_caixa = 1.80 * ( max_alt_larg * 2 + raio_interno * 2) 
p_central_y = largura_caixa / 2

[outTotal, nTotal] = read_out_elipse(out_name, 0.072,0.072,0.072,0.072, 0.03*0.9, 0.03*0.9);

divx = sqrt(nTotal*1.2);

[alvo Nalvos] = gAlvoConstElip(0.2, 0.2, ceil(divx), ceil(divx), 0.072,0.072,0.072,0.072, 0.03, 0.03);
%writeAlvo(alvo_name, alvo);
[bx, by, tve] = TVE(alvo, outTotal);