clear 
clear all
clc

auxiliar_script_path = "../scripts_auxiliares/";

source([auxiliar_script_path "gera_pre.m"]);
source([auxiliar_script_path "gera_efmat.m"]);
source([auxiliar_script_path "read_cfg.m"]);

path = get_path_cfg();
vreturn = read_cfg(path);

p1_caixa = [0 ; 0];

nome_simulacao = char(vreturn(1));
path_simulacao = char(vreturn(3));
unit = 1;
altura_ima = cell2mat(vreturn(18));
largura_ima = cell2mat(vreturn(17));
raio_interno = cell2mat(vreturn(20));
ang_ini_mag = - (0)*pi/2;
ang_ini_ima = - 0*pi/2;
fator_giro = 0.5;
n_imas = cell2mat(vreturn(10));
mod_B = cell2mat(vreturn(14)); % em tesla
k = 2;
h = figure();
max_alt_larg = max (altura_ima, largura_ima);

altura_caixa = 1.20 * ( max_alt_larg * 2 + raio_interno * 2) 
p_central_x = altura_caixa / 2

largura_caixa = 1.20 * ( max_alt_larg * 2 + raio_interno * 2) 
p_central_y = largura_caixa / 2

p2_caixa = [0 ; altura_caixa];
p3_caixa = [largura_caixa ; altura_caixa];
p4_caixa = [largura_caixa ; 0];


if n_imas == 0
  theta = 2 * asin((largura_ima / 2) / raio_interno); % ang em rad
  n_imas = round(2 * pi / theta); 
  theta = 2 * pi / (n_imas);
else
  theta = 2 * pi / (n_imas); 
end

%  p2 .------. p3
%     |      |
%     |      |
%  p1 !______! p4


p1_ima(1,1) = - largura_ima / 2;
p1_ima(1,2) = - altura_ima / 2;

p2_ima(1,1) = p1_ima(1,1);
p2_ima(1,2) = p1_ima(1,2) + altura_ima;

p3_ima(1,1) = p2_ima(1,1) + largura_ima;
p3_ima(1,2) = p2_ima(1,2);

p4_ima(1,1) = p3_ima(1,1);
p4_ima(1,2) = p3_ima(1,2) - altura_ima;


translation_point = [p_central_x; p_central_y];
ang = + ang_ini_ima + (0:n_imas-1) * theta;
ang_mag(1) = 0 * fator_giro * k + ang_ini_mag;

TP(:,1) = (max_alt_larg/2 + raio_interno) * cos(ang);
TP(:,2) = (max_alt_larg/2 + raio_interno) * sin(ang);

TP1(1,:) = TP(1,:) + translation_point';

Br(1) = mod_B * sin( ang_mag );
Btheta(1) = - mod_B * cos ( ang_mag); 
%Bx(1) = cos(ang(1)) * Br(1) - sin(ang(1)) * Btheta(1);
%By(1) = sin(ang(1)) * Br(1) + cos(ang(1)) * Btheta(1);
%Bx(1) = mod_B * sin( 2 * ang_mag); 
%By(1) = - mod_B * cos( 2 * ang_mag);
Bx(1) = mod_B * (cos(ang(1)) * sin(ang_mag) + cos(ang_mag) * sin(ang(1)));
By(1) = mod_B * (sin(ang(1)) * sin(ang_mag) - cos(ang_mag) * cos(ang(1)));


for i=2:n_imas

matriz = [cos( ang(i)) sin( ang(i)); -sin( ang(i)) cos( ang(i))];

ang_mag(i) = ang(i) * fator_giro * k + ang_ini_mag;

%primeiro gira o ima
matriz_mag = [cos( ang_mag(i)) sin( ang_mag(i)); -sin( ang_mag(i)) cos( ang_mag(i))];
p1_ima(i,:) = (matriz_mag * p1_ima(1,:)')' + translation_point' + TP(i,:);
p2_ima(i,:) = (matriz_mag * p2_ima(1,:)')' + translation_point' + TP(i,:);
p3_ima(i,:) = (matriz_mag * p3_ima(1,:)')' + translation_point' + TP(i,:);
p4_ima(i,:) = (matriz_mag * p4_ima(1,:)')' + translation_point' + TP(i,:);

%p1_ima(i,:) = (matriz * p1_ima(1,:)')' + translation_point' + TP(i,:);
%p2_ima(i,:) = (matriz * p2_ima(1,:)')' + translation_point' + TP(i,:);
%p3_ima(i,:) = (matriz * p3_ima(1,:)')' + translation_point' + TP(i,:);
%p4_ima(i,:) = (matriz * p4_ima(1,:)')' + translation_point' + TP(i,:);
TP1(i,:) = TP(i,:) + translation_point';


Br(i) = mod_B * sin( ang_mag(i) );
Btheta(i) = - mod_B * cos ( ang_mag(i));
%ang(i) = ang_mag;
%Bx(i) = cos( ang(i) ) * Br(i) - sin( ang(i) ) * Btheta(i);
%By(i) = sin( ang(i) ) * Br(i) + cos( ang(i) ) * Btheta(i);
%Bx(i) = mod_B * sin( 2 * ang_mag); 
%By(i) = - mod_B * cos( 2 * ang_mag);

Bx(i) = mod_B * (cos(ang(i)) * sin(ang_mag(i)) + cos(ang_mag(i)) * sin(ang(i)));
By(i) = mod_B * (sin(ang(i)) * sin(ang_mag(i)) - cos(ang_mag(i)) * cos(ang(i)));
end

mod_factor = 0.003;

matriz_mag = [cos( ang_mag(1)) sin( ang_mag(1)); -sin( ang_mag(1)) cos( ang_mag(1))];
p1_ima(1,:) = (matriz_mag * p1_ima(1,:)')';
p2_ima(1,:) = (matriz_mag * p2_ima(1,:)')';
p3_ima(1,:) = (matriz_mag * p3_ima(1,:)')';
p4_ima(1,:) = (matriz_mag * p4_ima(1,:)')';

p1_ima(1,:) = p1_ima(1,:) + translation_point' + TP(1,:);
p2_ima(1,:) = p2_ima(1,:) + translation_point' + TP(1,:);
p3_ima(1,:) = p3_ima(1,:) + translation_point' + TP(1,:);
p4_ima(1,:) = p4_ima(1,:) + translation_point' + TP(1,:);

%plot([TP1(:,1);TP1(1,1)],[TP1(:,2);TP1(1,2)])
hold on
plot(p_central_x,p_central_y)

for i = 1:n_imas
  if mod( i, 4) == 0
    param = 'k';
  elseif mod(i,4) == 1
    param = 'r';
  elseif mod(i,4) == 2
    param = 'b';
  else
    param = 'g';
  end
  plot(p1_ima(i,1),p1_ima(i,2),strcat(param,'*'));
  plot(p2_ima(i,1),p2_ima(i,2),strcat(param,'*'));
  plot(p3_ima(i,1),p3_ima(i,2),strcat(param,'*'));
  plot(p4_ima(i,1),p4_ima(i,2),strcat(param,'*'));
  plot([p1_ima(i,1) p2_ima(i,1) p3_ima(i,1) p4_ima(i,1) p1_ima(i,1)], 
  [p1_ima(i,2) p2_ima(i,2) p3_ima(i,2) p4_ima(i,2) p1_ima(i,2)],param);

  quiver(TP1(i,1),TP1(i,2),Bx(i)*mod_factor, By(i)*mod_factor,param)
  text(TP1(i,1),TP1(i,2), sprintf("%d",i));
end

text(p_central_x - raio_interno/2,p_central_y + 2, sprintf("(%0.2f, %0.2f)",p_central_x,p_central_y));
plot([p1_caixa(1) p2_caixa(1) p3_caixa(1) p4_caixa(1)],[p1_caixa(2) p2_caixa(2) p3_caixa(2) p4_caixa(2)],'k-');
xlim([p1_caixa(1) p3_caixa(1)]);
ylim([p1_caixa(2) p3_caixa(2)]);
title("Domínio de Cálculo para problema de elementos finitos de análise de halbach Array Circular");
xlabel("m");
ylabel("m");

hold off

print("dominio_calculo_halbach_circular","-deps");
gera_efmat(sprintf("%sefmat.dat",path_simulacao),n_imas,Bx,By,auxiliar_script_path);
gera_pre(sprintf("%s%s.pre", path_simulacao, nome_simulacao),unit,p1_ima,p2_ima,p3_ima,p4_ima,p1_caixa,p2_caixa,p3_caixa,p4_caixa,
ang_ini_ima);