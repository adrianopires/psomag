% script principal para rodar o algoritmo de otimizacao com base em um campo alvo conhecido
clear all;
clear
clc 
auxiliar_script_path = "../scripts_auxiliares/";

source([auxiliar_script_path "inputsLib.m"]);
source([auxiliar_script_path "alvoLib.m"]);

simulation = "halbach";
pathname_resultado = [simulation ".out"];
pathname_alvo = [simulation ".alvo"];
pathname_input_efcs = ["input_efcs_" simulation ".input"];
pathname_input_efcsouts = ["input_efcsouts_" simulation ".input"];
