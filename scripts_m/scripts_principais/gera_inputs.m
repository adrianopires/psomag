% script principal para rodar o algoritmo de otimizacao com base em um campo alvo conhecido
clear all;
clear
clc 
auxiliar_script_path = "../scripts_auxiliares/";

source([auxiliar_script_path "inputsLib.m"]);
source([auxiliar_script_path "alvoLib.m"]);
source([auxiliar_script_path "read_cfg.m"]);

path = get_path_cfg();
vreturn = read_cfg(path);

simulation = char(vreturn(1));
pathname_resultado = char(vreturn(6));
pathname_alvo = char(vreturn(7));
pathname_input_efcs = char(vreturn(4));
pathname_input_efcsouts = char(vreturn(5));

altura_ima = cell2mat(vreturn(18));
largura_ima = cell2mat(vreturn(17));
raio_interno = cell2mat(vreturn(20));
Bxconst = cell2mat(vreturn(14));
Byconst = cell2mat(vreturn(15));
divx = cell2mat(vreturn(21));
divy = cell2mat(vreturn(22));

max_alt_larg = max (altura_ima, largura_ima);

altura_caixa = 1.20 * ( max_alt_larg * 2 + raio_interno * 2);
p_central_x = altura_caixa / 2;
largura_caixa = 1.20 * ( max_alt_larg * 2 + raio_interno * 2);
p_central_y = largura_caixa / 2;

%[alvo Nalvos] = gAlvoConstElip(Bxconst, Byconst, ceil(divx), ceil(divx), p_central_x,p_central_y,p_central_x,p_central_y, raio_interno, raio_interno);
%writeAlvo(pathname_alvo, alvo);

if (gera_input_efcs(pathname_input_efcs, simulation) < 0)
	printf("Erro ao gerar input EFCS");
	return;
end

if (gera_input_efcsouts(pathname_input_efcsouts, simulation, pathname_resultado) < 0)
	printf("Erro ao gerar input EFOUTS");
	return;
end