% script principal para rodar o algoritmo de otimizacao com base em um campo alvo conhecido
clear all;
clear
clc 
auxiliar_script_path = "../scripts_auxiliares/";

source([auxiliar_script_path "inputsLib.m"]);
source([auxiliar_script_path "alvoLib.m"]);
source([auxiliar_script_path "readLib.m"]);
source([auxiliar_script_path "read_cfg.m"]);

path = get_path_cfg();
vreturn = read_cfg(path);

% entrada de dados sistema
simulation = char(vreturn(1));
pathname_resultado = char(vreturn(6));
pathname_resultado_for_alvo = char(vreturn(23));
pathname_alvo = char(vreturn(7));
pathname_input_efcs = char(vreturn(4));
pathname_input_efcsouts = char(vreturn(5));

% entrada de dados imas
altura_ima = cell2mat(vreturn(18));
largura_ima = cell2mat(vreturn(17));
raio_interno = cell2mat(vreturn(20));

x1 = cell2mat(vreturn(24));
y1 = cell2mat(vreturn(25));
x2 = cell2mat(vreturn(26));
y2 = cell2mat(vreturn(27));
a = cell2mat(vreturn(28));
b = cell2mat(vreturn(29));

[alvo, nAlvo] = gAlvoFromPathElipse(pathname_resultado_for_alvo, \
                x1,y1,x2,y2,a,b);

writeAlvo(pathname_alvo, alvo)

if (gera_input_efcs(pathname_input_efcs, simulation) < 0)
	printf("Erro ao gerar input EFCS");
	return;
end

if (gera_input_efcsouts(pathname_input_efcsouts, simulation, pathname_resultado) < 0)
	printf("Erro ao gerar input EFOUTS");
	return;
end