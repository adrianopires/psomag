% script principal para rodar o algoritmo de otimizacao com base em um campo
% alvo conhecido

% preambulo
clear all;
clear
clc 

auxiliar_script_path = "../scripts_auxiliares/";

source([auxiliar_script_path "gera_efmat.m"]);
source([auxiliar_script_path "readLib.m"]);
source([auxiliar_script_path "generalLib.m"]);

% entrada de dados sistema
simulation = "halbach";
pathname_resultado = [simulation ".out"];
pathname_alvo = [simulation ".alvo"];
pathname_efmat = "efmat.dat";
pathname_input_efcs = ["input_efcs_" simulation ".input"];
pathname_input_efcsouts = ["input_efcsouts_" simulation ".input"];
efcs_exec = "efcs < ";
efcsouts_exec = "efcsouts < ";

%entrada de dados imas
n_ima = 12;
Br_mod = 1.14; % em tesla
unit = 1e-3;
altura_ima = 10*unit; % em mm
largura_ima = 10*unit; % em mm
raio_interno = 25*unit; % em mm
max_alt_larg = max (altura_ima, largura_ima);
altura_caixa = 1.20 * ( max_alt_larg * 2 + raio_interno * 2) 
p_central_x = altura_caixa / 2
largura_caixa = 1.20 * ( max_alt_larg * 2 + raio_interno * 2) 
p_central_y = largura_caixa / 2;

%entrada de dados do PSO
n_max_iteracoes = 2e3;
max_value = pi;
min_value = -pi;

[alvo Nalvo] = read_alvo(pathname_alvo);
percentage_alvo = 1;

% trecho do algoritmo PSO
C1 = 1;
C2 = 1;
R1 = rand(n_ima);
R2 = rand(n_ima);
N = 15; % Quantidade populacional de particulas
V = rand() * ones(N,n_ima); % inicializa as velocidades randomicamente;
X = rand(N,n_ima) * max_value; % inicializa posicoes iniciais;
Pbest = rand() * ones(N,n_ima);
Gbest = rand() * ones(1,n_ima);
tveGbest = inf;
tvePbest = ones(N,1) * inf;
t = 0;
while (t < n_max_iteracoes)
  for (i=1:N)
    
    % calcula as velocidades e as posicoes
    for (j=1:n_ima)
      V(i,j) = V(i,j) + C1*R1(j)*(Pbest(i,j) - X(i,j)) + \
      C2*R2(j)*(Gbest(j) - X(i,j));
      temp = X(i,j) + V(i,j);
      %if temp < max_value && temp > min_value
	X(i,j) = temp;
      %end
    end
    
    time1 = time();
    Bx = Br_mod * sin(X(i,:));
    By = Br_mod * cos(X(i,:));
    ret_efmat = gera_efmat(pathname_efmat, n_ima, Bx, By, auxiliar_script_path);
    time2 = time();
    %printf("tempo para gerar efmat = %f\n", time2-time1);
    
    time1 = time();
    if (exec_EFCAD([efcs_exec pathname_input_efcs], [efcsouts_exec pathname_input_efcsouts]) < 0)
      return;
    end
    time2 = time();
    %printf("tempo para rodar efcad = %f\n", time2-time1);
    
    time1=time();
    [outTotal, nTotal] = read_out_elipse(pathname_resultado, \
    p_central_x, p_central_y, p_central_x, p_central_y, \
    raio_interno*percentage_alvo, raio_interno*percentage_alvo);
    time2 = time();
    %printf("tempo para ler saida = %f\n", time2-time1);
    
    time1 = time();
    [bxX, byX, tveX] = TVE(alvo, outTotal);
    clear outTotal
    time2 = time();
    %printf("tempo para calcular erro = %f\n", time2-time1);
    
    %Bx = Br_mod * sin(Pbest(i,:));
    %By = Br_mod * cos(Pbest(i,:));
    %ret_efmat = gera_efmat(pathname_efmat, n_ima, Bx, By, auxiliar_script_path);
    
    %if (exec_EFCAD([efcs_exec pathname_input_efcs], \
    %  [efcsouts_exec pathname_input_efcsouts]) < 0)
    %  return;
    %end
    
    %[outTotal, nTotal] = read_out_elipse(pathname_resultado, \
    %p_central_x, p_central_y, p_central_x, p_central_y, \
    %raio_interno*percentage_alvo, raio_interno*percentage_alvo);

    %[bxPbest(i), byPbest(i), tvePbest(i)] = TVE(alvo, outTotal);
    clear outTotal
    
    if (tvePbest(i) > tveX)
      tvePbest(i) = tveX;
      Pbest(i,:) = X(i,:);
      
      name_pbest_simu = sprintf("%s_pbest%d",simulation,i);
      copy_simulation(simulation, name_pbest_simu);
      
      %Bx = Br_mod * sin(Gbest);
      %By = Br_mod * cos(Gbest);
      %ret_efmat = gera_efmat(pathname_efmat, n_ima, \
      %Bx, By,auxiliar_script_path);
      
      %if (exec_EFCAD([efcs_exec pathname_input_efcs], \
%	     [efcsouts_exec pathname_input_efcsouts]) < 0)
%	      return;
%      end
    
%      [outTotal, nTotal] = read_out_elipse(pathname_resultado, \ 
%      p_central_x, p_central_y, p_central_x, p_central_y, \
%      raio_interno*percentage_alvo, raio_interno*percentage_alvo);

 %     [bxGbest, byGbest, tveGbest] = TVE(alvo, outTotal);
 %     clear outTotal
      if (tveGbest > tvePbest(i))
	     Gbest = Pbest(i,:);
	     tveGbest = tvePbest(i);
	     name_gbest_simu = sprintf("%s_gbest",simulation);
	     copy_simulation(simulation, name_gbest_simu);
      end
    end
  end
  t = t + 1;
  convergencia(t) = (max(tvePbest) - tveGbest)/max(tvePbest);
  printf("convergencia = %d porcento, iteracao: %d",convergencia(t) * 100, t);
  if (convergencia(t) < 0.01 && t > 100)
    printf("Criterio de parada: convergencia = %g porcento, iteracao: %d",convergencia(t) * 100, t);
    break;
  end
  write_resumo_simu(tvePbest, tveGbest);
end


Bx = Br_mod * sin(Gbest);
By = Br_mod * cos(Gbest);
ret_efmat = gera_efmat(pathname_efmat, n_ima, Bx, By,auxiliar_script_path);
if (exec_EFCAD([efcs_exec pathname_input_efcs], [efcsouts_exec pathname_input_efcsouts]) < 0)
  return;
end
    
[outTotal, nTotal] = read_out_elipse(pathname_resultado, \
p_central_x, p_central_y, p_central_x, p_central_y, \
raio_interno*percentage_alvo, raio_interno*percentage_alvo);

[bxGbest, byGbest, tveGbest] = TVE(alvo, outTotal);