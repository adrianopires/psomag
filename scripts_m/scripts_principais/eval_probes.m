% script to get the probe points and save a magnetic file output
% also show TVE of field and error distribution graph

clear all;
clear
clc 
auxiliar_script_path = "../scripts_auxiliares/";

source([auxiliar_script_path "inputsLib.m"]);
source([auxiliar_script_path "alvoLib.m"]);
source([auxiliar_script_path "readLib.m"]);
source([auxiliar_script_path "generalLib.m"]);
source([auxiliar_script_path "read_cfg.m"]);

path = get_path_cfg();
vreturn = read_cfg(path);
[n_probes probesx probesy]= read_probes(path);

simulation = char(vreturn(1));
simulation_main_path = char(vreturn(2));
pathname_resultado = char(vreturn(6));
pathname_alvo = char(vreturn(7));

[outTotal nTotal] = read_out_all([simulation_main_path "Gbest/" pathname_resultado]);
[alvos Nalvo] = read_alvo(pathname_alvo);
limiar = 1e-3; % 1mm de distancia do ponto desejado

bx = 0;
by = 0;
bx_alvo = 0;
by_alvo = 0;
ex = 0;
ey = 0;
erx = 0;
ery = 0;
printf("Probe i (x,y) = (percentage_error_Bx, percentage_error_By\n");
for (i=1:n_probes)
  [bx(i) by(i)] = eval_probe(probesx(i), probesy(i), outTotal);
  [bx_alvo(i) by_alvo(i)] = eval_probe_alvo(probesx(i), probesy(i), alvos, limiar);
  ex(i) = (bx_alvo(i) - bx(i));
  erx(i) = ex(i) / bx_alvo(i); 
  ey(i) = (by_alvo(i) - by(i));
  ery(i) = ey(i) / by_alvo(i); 
  printf("Probe %d (%f, %f) = (%f, %f)\n", i, probesx(i), probesy(i), \
  erx(i) * 100, ery(i) * 100);
end
