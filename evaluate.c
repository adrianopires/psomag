#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include "evaluate.h"
#include "pso.h"
#include "cfg.h"
#include "efcad_aux.h"
#include "read.h"
#include "el_alvo.h"
#include <pthread.h>


/******************************************************
 *
 *
 * internal definitiions
 *
 *
******************************************************/

#define USE_SCRITPS 1

extern pthread_mutex_t lock;
/******************************************************
 *
 *
 * internal functions
 *
 *
******************************************************/

static double eval_calc_tve(element_t *e,
			    alvo_t *a,
			    int size_alvo,
			    int size_out_in) {

  int i, j;
  double tve = 0;

  for (i = 0; i < size_alvo; i++) {
    for (j = 0; j < size_out_in; j++) {
      if (is_alvo_in_element(a[i], e[j])) {
	tve = tve + sqrt((a[i].bx - e[j].bx) * (a[i].bx - e[j].bx));
	tve = tve + sqrt((a[i].by - e[j].by) * (a[i].by - e[j].by));
      }
    }
  }

  if (tve == 0) {
    printf("\n\n\n TVE == 0.000000 ... Possible error! \n\n\n");
  }
  return tve;
}

static double eval_elipse(pso_particle_t p, cfg_t cfg, int particle) {
  char path[256];
  element_t *eo;
  alvo_t *al;
  double result;
  int size_alvo, size_out, size_out_in;
  int i;

  eo = malloc(sizeof(element_t));
  al = malloc(sizeof(alvo_t));
  
  sprintf(&path[0], "%s", cfg.alvo_path);
  size_alvo = read_csv_alvo(path, &al);
  if (size_alvo < 1) {
    printf("Size_alvo too small!!!\n\n");
    return INFINITY;
  }

  sprintf(&path[0], "%03d/%s.out", particle, cfg.sim_name);
  size_out = read_csv_out(path, &eo);
  if (size_out < 1) {
    printf("Size_out too small!!!\n\n");
    return INFINITY;
  }
  
  size_out_in = return_elements_in_elipse(&eo, eo, cfg, size_out);
  if (size_out_in < 1) {
    printf("Size_out_in too small!!!\n\n");
    return INFINITY;
  }

  result = eval_calc_tve(eo, al, size_alvo, size_out_in);
  
  free(al);
  free(eo);
  return result;
}

static double eval_cost_rastrigin(pso_particle_t p, cfg_t cfg) {
  int i;
  double sum;
  sum = 10.0 * cfg.n_var;
  for (i = 0; i < cfg.n_var; i++) {
    sum = sum + (p.x[i] * p.x[i] - 10.0 * cos(2 * M_PI * p.x[i]));
  }
  return sum;
}

static double eval_cost_x2(pso_particle_t p, cfg_t cfg) {
  return p.x[0] * p.x[0];
}

static double eval_cost_xy2(pso_particle_t p, cfg_t cfg) {
  return p.x[0] * p.x[0] + p.x[1] * p.x[1];
}

static double eval_cost_halbach(pso_particle_t p, cfg_t cfg, int particle) {
  double result;
  
  if (aux_gera_efmat(p, cfg, particle) < 0) {
    printf("Error generating efmat.\n");
    return -1;
  }
  #ifndef USE_SCRITPS
  if (aux_exec_efcs(p, cfg, particle) < 0) {
    printf("Error executing efcs.\n");
    return -1;
  }

  if (aux_exec_efcsouts(p, cfg, particle) < 0) {
    printf("Error executing efcsouts.\n");
    return -1;
  }
  #else
  if (aux_exec_script(p, cfg, particle) < 0) {
   printf("Error executing script.\n");
   return -1;
  }
  #endif
  pthread_mutex_lock(&lock);
  result = eval_elipse(p, cfg, particle);
  pthread_mutex_unlock(&lock);
  
  return result;
}

/******************************************************
 *
 *
 * external functions
 *
 *
******************************************************/
double eval_cost_function(pso_particle_t *p, cfg_t *cfg, int particle) {
  return eval_cost_halbach(*p, *cfg, particle);
  //return eval_cost_rastrigin(*p, *cfg);
}
