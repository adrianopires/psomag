#ifndef __EFCAD_AUX__
#define __EFCAD_AUX__

#include "pso.h"
#include "cfg.h"

int aux_exec_script(pso_particle_t p, cfg_t cfg, int particle);
int aux_gera_efmat(pso_particle_t p, cfg_t cfg, int particle);
int aux_exec_efcs(pso_particle_t p, cfg_t cfg, int particle);
int aux_exec_efcsouts(pso_particle_t p, cfg_t cfg, int particle);

#endif
