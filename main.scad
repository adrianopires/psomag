$fn=50;

include <imas.scad>
include <probe.scad>

// sensor dimensions
sensor_h = 3.2;
sensor_l = 4.2;
sensor_w = 1.6;
sensor_diameter=sensor_l;
with_probes=1;

module ima(size) {
	//translating put ima in center 
	translate(-[size/2, size/2, 0]){
		cube([size,size,size * 1.05]);
	}
}

module main_cylinder(height, inner_r, outer_r){
	// translating align imas and plate
	translate([0,0,-height / 2]) {
		difference(){
			cylinder(height,
				outer_r, outer_r);
			cylinder(height, 
				inner_r, inner_r);
		}
	}
}

difference() {
	main_cylinder(cld_h, 0, cld_or);
	for ( i = [0:n_imas - 1] ) {
		rotate( 360 * (i / n_imas)) {
			translate(vec) {
				rotate(-imas_angle[i]) {
                  translate([0,0, -1]) {
                    linear_extrude(1){
                        text(str(i), 4);
                    }
                   }

					ima(ima_size);
				}
			}
		}
	}

     if (with_probes) {
		translate([0,0, ima_size / 2 + sensor_h / 2]) {
			cylinder(cld_h, cld_ir,cld_ir);
		}

		for ( j = [0:n_eval_probes - 1]) {
			translate(eval_probes[j]) {
				translate([0, 0, ima_size/2 - (sensor_h / 2) * 1.1]) {
                    translate([0,0, -1]) {
                        linear_extrude(1){
                            text(str(j), 2);
                        }
                    }
					drop(sensor_h * 10, sensor_l * 0.75);
					rotate(270) {
						drop(sensor_h * 10, sensor_l * 0.75);
					}
				}
			}
		}
	} else {
		main_cylinder(cld_h, 0, cld_ir);
	}
}