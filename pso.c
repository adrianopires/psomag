#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include "PSOmag.h"
#include "pso.h"

/******************************************************
 *
 *
 * internal definitiions
 *
 *
******************************************************/

#define EFCS_PATH "/Users/adrianoop/EFCAD/efcs"
#define EFCSOUTS_PATH "/Users/adrianoop/EFCAD/efcsouts"


/******************************************************
 *
 *
 * internal functions
 *
 *
******************************************************/
static double wrap_angle(double angle) {
  while (angle > M_PI) {
    angle = angle - 2 * M_PI;
  }
  while (angle < - M_PI) {
    angle = angle + 2 * M_PI;
  }
  
  return angle;
}
static int pso_position_update_angle(pso_particle_t *self, int n_var) {
  int i;
  double temp;
  for (i = 0; i < n_var; i++) {
    temp = self->x[i] + self->v[i];
    self->x[i] = wrap_angle(temp);
  }
  return 0;
}

static int pso_alloc_coef(pso_particle_t * self, int n_var) {
  self->r1 = malloc(n_var * sizeof(double));
  if (self->r1 == NULL) {
    return -1;
  }

  self->r2 = malloc(n_var * sizeof(double));
  if (self->r2 == NULL) {
    return -1;
  }

  self->c1 = malloc(n_var * sizeof(double));
  if (self->c1 == NULL) {
    return -1;
  }

  self->c2 = malloc(n_var * sizeof(double));
  if (self->c2 == NULL) {
    return -1;
  }
  
  self->x = malloc(n_var * sizeof(double));
  if (self->x == NULL) {
    return -1;
  }

  self->v = malloc(n_var * sizeof(double));
  if (self->v == NULL) {
    return -1;
  }

  self->w = malloc(n_var * sizeof(double));
  if (self->w == NULL) {
    return -1;
  }

  return 0;
}

static int pso_init_vector_random(int size, double *coef) {
  int i;
  // initialize all coeficients between [-1,+1]
  // with .3 precision
  for (i = 0; i < size; i++) {
    coef[i] = ((rand() % 2000) / 1e3 - 1);
  }
  return 0;
}

static int pso_init_vector_value(int size, double *coef, double value) {
  int i;
  for (i = 0; i < size; i++) {
    coef[i] = value;
  }
  return 0;
}

static int pso_init_pos_random(pso_particle_t *self, int n_var, double xmin, double xmax) {
  int i;
  if (pso_init_vector_random(n_var, self->x) < 0) {
    return -1;
  }

  for (i = 0; i < n_var; i++) {
    self->x[i] = self->x[i] * xmax;
    
    if (self->x[i] > xmax) {
      self->x[i] = xmax;
    }
    if (self->x[i] < xmin) {
      self->x[i] = xmin;
    }
  }
  return 0;
}

static int pso_create_script(cfg_t cfg, int particle) {
  char buf[100];
  FILE *fp;

  sprintf(&buf[0], "script%03d.sh", particle);
  fp = fopen(buf, "w");
  if (fp == NULL) {
    return -1;
  }

  fprintf(fp, "cd %03d\n", particle);
  fprintf(fp, "%s < %s > /dev/null\n", EFCS_PATH, cfg.input_efcs);
  fprintf(fp, "%s < %s > /dev/null\n", EFCSOUTS_PATH, cfg.input_efcsouts);
  fclose(fp);

  sprintf(&buf[0], "chmod +x script%03d.sh", particle);
  system(buf);
  return 0;
}


static int pso_create_infraestructure(cfg_t cfg, int particle) {
  char buf[100];
  sprintf(&buf[0], "mkdir %03d", particle);
  system(buf);
  sprintf(&buf[0], "cp %s.* %03d", cfg.sim_name, particle);
  system(buf);
  // prepare directory for Pbest particles
  sprintf(&buf[0], "mkdir Pbest%03d", particle);
  system(buf);

  if (pso_create_script(cfg, particle) < 0) {
    return -1;
  }
  
  return 0;
}

static int henon_update(int n_var, pso_particle_t *self) {
  // as self->r1[i] and self->r2[i] init as random
  // is possible to go through with henon update
  // without further conserns by initial values

  int i;
  double b = 0.3, a = 1.5;
  double y1, y2;
  
  for (i = 0; i < n_var; i++) {
    y1 = b * self->r1[i];
    self->r1[i] = 1 + abs(y1) - a * self->r1[i] * self->r1[i];

    y2 = b * self->r2[i];
    self->r2[i] = 1 + abs(y2) - a * self->r2[i] * self->r2[i];
  }
  return 0;
}

static int pso_henon_velocity_update(pso_particle_t *self,
				     pso_particle_t Pbest,
				     pso_particle_t Gbest,
				     int n_var) {
  int i;

  if (henon_update(n_var, self) < 0) {
    return -1;
  }
  
  for (i = 0; i < n_var; i++) {
    self->v[i] = self->w[i] * self->v[i] +
      self->c1[i] * self->r1[i] * (Pbest.x[i] - self->x[i]) +
      self->c2[i] * self->r2[i] * (Gbest.x[i] - self->x[i]);
  }
  return 0;
}

static int pso_random_velocity_update(pso_particle_t *self,
				      pso_particle_t Pbest,
				      pso_particle_t Gbest,
				      int n_var) {
  int i;

  if ((pso_init_vector_random(n_var, self->r1) < 0) ||
      (pso_init_vector_random(n_var, self->r2) < 0)) {
    printf("Error randomizing coeficients.\n"); 
    return -1;
  }
  
  for (i = 0; i < n_var; i++) {
    self->v[i] = self->w[i] * self->v[i] +
      self->c1[i] * self->r1[i] * (Pbest.x[i] - self->x[i]) +
      self->c2[i] * self->r2[i] * (Gbest.x[i] - self->x[i]);
  }
  return 0;
} 

static int pso_stable_velocity_update(pso_particle_t *self,
				      pso_particle_t Pbest,
				      pso_particle_t Gbest,
				      int n_var) {
  int i;
  for (i = 0; i < n_var; i++) {
    self->v[i] = self->w[i] * self->v[i] +
      self->c1[i] * self->r1[i] * (Pbest.x[i] - self->x[i]) +
      self->c2[i] * self->r2[i] * (Gbest.x[i] - self->x[i]);
  }
  return 0;
}

/******************************************************
 *
 *
 * external functions
 *
 *
******************************************************/
int pso_initialize_particles(PSOmag_t *self) {
  int i;
  char buf[100];
  srand((unsigned)time(NULL)); // change the rand() seed
  
  self->Pbest = malloc(self->cfg.n * sizeof (pso_particle_t));
  if (self->Pbest == NULL) {
    return -1;
  }
  bzero(self->Pbest, self->cfg.n * sizeof (pso_particle_t));

  self->costPbest = malloc(self->cfg.n * sizeof(double));
  if (self->costPbest == NULL) {
    return -1;
  }
    
  self->p = malloc(self->cfg.n * sizeof (pso_particle_t));
  if (self->p == NULL) {
    return -1;
  }
  bzero(self->p, self->cfg.n * sizeof (pso_particle_t));
  
  for (i = 0; i < self->cfg.n; i++) {
    // create directory for commom particles
    if (pso_create_infraestructure(self->cfg, i) < 0) {
      printf("Error creating infraestructure.\n");
      return -1;
    }

    if (pso_initialize_coef(&self->p[i], self->cfg) < 0) {
      printf("Error initializing particle %d\n", i);
      return -1;
    }

    if (pso_initialize_coef(&self->Pbest[i], self->cfg) < 0) {
      printf("Error initializing particle Pbest %d\n", i);
      return -1;
    }
    
    if (pso_init_pos_random(&self->p[i], self->cfg.n_var, self->cfg.xmin, self->cfg.xmax) < 0) {
      printf("Error initializing position %d\n", i);
      return -1;
    }

    if (pso_init_pos_random(&self->Pbest[i], self->cfg.n_var, self->cfg.xmin, self->cfg.xmax) < 0) {
      printf("Error initializing position Pbest %d\n", i);
      return -1;
    }
    
    self->costPbest[i] = INFINITY;
  }
  
  // prepare directory for Pbest particles
  sprintf(&buf[0], "mkdir Gbest");
  system(buf);
  self->costGbest = INFINITY;
  bzero(&self->Gbest, sizeof (pso_particle_t));

  if (pso_initialize_coef(&self->Gbest, self->cfg) < 0) {
    printf("Error initializing particle Gbest\n");
    return -1;
  }

  if (pso_init_pos_random(&self->Gbest, self->cfg.n_var, self->cfg.xmin, self->cfg.xmax) < 0) {
      printf("Error initializing position Gbest\n");
      return -1;
    }
  
  return 0;
}

int pso_initialize_coef(pso_particle_t *self, cfg_t cfg) {
  if (pso_alloc_coef(self, cfg.n_var) < 0) {
    printf("Coeficients allocation Error");
    return -1;
  }

  if (pso_init_vector_random(cfg.n_var, self->r1) < 0) {
    printf("Error init coef r1\n");
    return -1;
  }

  if (pso_init_vector_random(cfg.n_var, self->r2) < 0) {
    printf("Error init coef r2\n");
    return -1;
  }

  if (pso_init_vector_value(cfg.n_var, self->c1, 1.0) < 0) {
    printf("Error init coef c1\n");
    return -1;
  }

  if (pso_init_vector_value(cfg.n_var, self->c2, 1.0) < 0) {
    printf("Error init coef c2\n");
    return -1;
  }

  if (pso_init_vector_value(cfg.n_var, self->w, cfg.w) < 0) {
    printf("Error init coef w\n");
    return -1;
  }

  if (pso_init_vector_value(cfg.n_var, self->v, 0.0) < 0) {
    printf("Error init coef v\n");
    return -1;
  }

  return 0;
}

int pso_velocity_update(pso_particle_t *self,
			pso_particle_t Pbest,
			pso_particle_t Gbest,
			int n_var) {
  //return pso_stable_velocity_update(self, Pbest, Gbest, n_var);
  //return pso_random_velocity_update(self, Pbest, Gbest, n_var);
  return pso_henon_velocity_update(self, Pbest, Gbest, n_var);
}

int pso_position_update(pso_particle_t *self, int n_var) {
  return pso_position_update_angle(self, n_var);
}

int pso_particle_copy(pso_particle_t *dst,
		      pso_particle_t *src,
		      int n_var,
		      const char *str_dst,
		      const char *str_src) {
  int i;
  char cmd[100];
  
  for (i = 0; i < n_var; i++) {
    dst->r1[i] = src->r1[i];
    dst->r2[i] = src->r2[i];
    dst->c1[i] = src->c1[i];
    dst->c2[i] = src->c2[i];
    dst->x[i] = src->x[i];
    dst->v[i] = src->v[i];
  }

  // clean destiny directory
  sprintf(&cmd[0],"rm -rf %s/*", str_dst);
  system(cmd);

  // copy particle files
  sprintf(&cmd[0],"cp %s/* %s/", str_src, str_dst);
  system(cmd);
  return 0;
}
