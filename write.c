#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include "write.h"

/******************************************************
 *
 *
 * internal definitiions
 *
 *
******************************************************/



/******************************************************
 *
 *
 * internal functions
 *
 *
******************************************************/
static int wrt_convergence_header(FILE *fp, PSOmag_t *self) {
  int i;
  //write result's header
  fprintf(fp, "iteration,");
  for (i = 0; i < self->cfg.n; i++) {
    fprintf(fp, "Pbest[%d],", i);
  }
  fprintf(fp, "Gbest\n");
  return 0;
}

static int wrt_convergence_result(FILE *fp, PSOmag_t *self, int t) {
  int i;
  fprintf(fp, "%d,",t);
  for (i = 0; i < self->cfg.n; i++) {
    fprintf(fp, "%0.3g,", self->costPbest[i]);
  }
  fprintf(fp, "%0.3g\n", self->costGbest);
  return 0;
}

static int wrt_results_header(FILE *fp, PSOmag_t *self) {
  int i;
  //write result's header
  fprintf(fp, "Particle");
  for (i = 0; i < self->cfg.n_var; i++) {
    fprintf(fp, ",X[%d]", i);
  }
  fprintf(fp, "\n");
  return 0;
}

static int wrt_results_result(FILE *fp, PSOmag_t *self) {
  int i, j;
  for (i = 0; i < self->cfg.n; i++) {
    fprintf(fp, "%d",i);
    for (j = 0; j < self->cfg.n_var; j++) {
      fprintf(fp, ",%0.3g", self->Pbest[i].x[j]);
    }
    fprintf(fp, "\n");
  }
  fprintf(fp, "Gbest");
  for (j = 0; j < self->cfg.n_var; j++) {
    fprintf(fp, ",%0.3g", self->Gbest.x[j]);
  }
  fprintf(fp, "\n");
  return 0;
}

/******************************************************
 *
 *
 * external functions
 *
 *
******************************************************/
int wrt_convergence(PSOmag_t *self, int t) {
  FILE *fp;
    
  fp = fopen("convergence.csv","a");
  if (fp == NULL) {
    return -1;
  }
  
  if (t == 0) {
    if (wrt_convergence_header(fp, self) < 0) {
      return -1;
    }
  }

  if (wrt_convergence_result(fp, self, t) < 0) {
    return -1;
  }
  fclose(fp);
  return 0;
}

int wrt_results(PSOmag_t *self) {
  FILE *fp;
  
  fp = fopen("results.csv","w");
  if (fp == NULL) {
    return -1;
  }
  
  if (wrt_results_header(fp, self) < 0) {
    return -1;
  }

  if (wrt_results_result(fp, self) < 0) {
    return -1;
  }

  fclose(fp);
  return 0;
}

int wrt_scad_file(PSOmag_t *self, const char *filepath) {
  FILE *fp;
  double unit = 1e3; //mm
  int i;
  
  fp = fopen(filepath,"w");

  if (fp == NULL) {
    return -1;
  }

  fprintf(fp,"n_imas = %d;\n", self->cfg.n_var);
  fprintf(fp,"backlash = %g; //mm\n", self->cfg.scad.backlash * unit);
  fprintf(fp,"real_ima_size = %g; //mm\n", self->cfg.scad.real_ima_size * unit);
  fprintf(fp,"real_inner_r = %g; //mm\n", self->cfg.scad.real_inner_r * unit);
  fprintf(fp,"imas_angle = [\n");
  for (i = 0; i < self->cfg.n_var; i++) {
    fprintf(fp,"%g,\n", (180/M_PI) * self->Gbest.x[i]);
  }
  fprintf(fp,"];\n");
  fprintf(fp,"ima_size = real_ima_size + backlash;\n");
  fprintf(fp,"thickness = sqrt(ima_size / 2) + %g;\n", self->cfg.scad.thickness * unit);
  fprintf(fp,"cld_h = ima_size * 2;\n");
  fprintf(fp,"cld_ir = real_inner_r - thickness;\n");
  fprintf(fp,"cld_or = cld_ir + ima_size + 2 * thickness;\n");
  fprintf(fp,"trans_r = cld_ir + (cld_or - cld_ir) / 2;\n");
  fprintf(fp,"vec = [trans_r, 0, 0];\n");
  fprintf(fp,"n_eval_probes = %d;\n", self->cfg.n_probes);
  fprintf(fp,"eval_probes = [\n");
  for (i = 0; i < self->cfg.n_probes; i++) {
    fprintf(fp,"[%f, %f, 0],\n",
	    self->cfg.probex[i] * unit,
	    self->cfg.probey[i] * unit);
  }
  fprintf(fp,"];\n");
  fclose(fp);
  
  return 0;
}

int wrt_cpy_to_path(char *filepath, char *path_dest) {
  char cmd[256];

  sprintf(&cmd[0],"cp %s %s", filepath, path_dest);
  if (system(cmd) < 0) {
    printf("error running %s\n", cmd);
  }
  
  return 0;
}
