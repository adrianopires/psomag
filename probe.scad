$fn=50;

module sensor(l, w, h) {
	points =	[
		[-l/2, -w/2], // 0
		[+l/2, -w/2], // 1
		[+l/2, 0 ], // 2
		[+l/2 - w/2,  +w/2], // 3
		[-l/2 + w/2,  +w/2], // 4
		[-l/2, 0] // 5
		];
  	linear_extrude(h) {
		polygon(points);
	}
}

module arrow(h, w) {
	points =	[
		[0,0], // 0
		[w,0], // 1
		[0,w] // 2
		];

  	linear_extrude(h) {
		polygon(points);
	}
}

module drop(cy_h, cy_r) {
	union() {	
		cylinder(cy_h, cy_r, cy_r);
		translate([0, cy_r * 1.3, 0]) {
			rotate(-135) {
				arrow(cy_h, cy_r);
			}
		}
	}
}

module probe(sensor_l, sensor_w, sensor_h) {
	difference() {
		drop(sensor_h, sensor_l * 0.75);
		rotate(180) {
			sensor(sensor_l, sensor_w, sensor_h);
		}
	}
}

// sensor dimensions
//s_h = 3.2;
//s_l = 4.2;
//s_w = 1.6;
// cylinder infos
//cy_h = sensor_h;
//cy_r = sensor_l;

//probe(s_l, s_w, s_h);