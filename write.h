#ifndef __WRITE__
#define __WRITE__

#include "PSOmag.h"

int wrt_convergence(PSOmag_t *self, int t);
int wrt_results(PSOmag_t *self);
int wrt_scad_file(PSOmag_t *self, const char *filepath);
int wrt_cpy_to_path(char *filepath, char *path_dest);
  
#endif
