#ifndef __PSO__
#define __PSO__


typedef struct particle{
  double *r1;
  double *r2;
  double *c1;
  double *c2;
  double *x;
  double *v;
  double *w;
} pso_particle_t;

#include "PSOmag.h"
#include "cfg.h"

int pso_initialize_particles(PSOmag_t *self);
int pso_initialize_coef(pso_particle_t *self, cfg_t cfg);
int pso_velocity_update(pso_particle_t *self,
			pso_particle_t Pbest,
			pso_particle_t Gbest,
			int n_var);
int pso_position_update(pso_particle_t *self, int n_var);
int pso_particle_copy(pso_particle_t *dst,
		      pso_particle_t *src,
		      int n_var,
		      const char *str_dst,
		      const char *str_src);

#endif
