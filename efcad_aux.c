#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <math.h>
#include "pso.h"
#include "cfg.h"

/******************************************************
 *
 *
 * internal definitiions
 *
 *
******************************************************/

#define LIMIT_MATERIAL 31
#define FIRST_LINE_COLUMNS 8
#define EFCS_PATH "/Users/adrianoop/EFCAD/efcs"
#define EFCSOUTS_PATH "/Users/adrianoop/EFCAD/efcsouts"
#define PID_TIMEOUT 10000 //ms

/******************************************************
 *
 *
 * internal functions
 *
 *
******************************************************/

static char *strrep(const char *s1, const char *s2, const char *s3) {
  if (!s1 || !s2 || !s3)
    return 0;
  size_t s1_len = strlen(s1);
  if (!s1_len)
    return (char *)s1;
  size_t s2_len = strlen(s2);
  if (!s2_len)
    return (char *)s1;

  /*
   * Two-pass approach: figure out how much space to allocate for
   * the new string, pre-allocate it, then perform replacement(s).
   */

  size_t count = 0;
  const char *p = s1;
  assert(s2_len); /* otherwise, strstr(s1,s2) will return s1. */
  do {
    p = strstr(p, s2);
    if (p) {
      p += s2_len;
      ++count;
    }
  } while (p);

  if (!count)
    return (char *)s1;

  /*
   * The following size arithmetic is extremely cautious, to guard
   * against size_t overflows.
   */
  assert(s1_len >= count * s2_len);
  assert(count);
  size_t s1_without_s2_len = s1_len - count * s2_len;
  size_t s3_len = strlen(s3);
  size_t newstr_len = s1_without_s2_len + count * s3_len;
  if (s3_len &&
      ((newstr_len <= s1_without_s2_len) || (newstr_len + 1 == 0)))
    /* Overflow. */
    return 0;

  char *newstr = (char *)malloc(newstr_len + 1); /* w/ terminator */
  if (!newstr)
    /* ENOMEM, but no good way to signal it. */
    return 0;

  char *dst = newstr;
  const char *start_substr = s1;
  size_t i;
  for (i = 0; i != count; ++i) {
    const char *end_substr = strstr(start_substr, s2);
    assert(end_substr);
    size_t substr_len = end_substr - start_substr;
    memcpy(dst, start_substr, substr_len);
    dst += substr_len;
    memcpy(dst, s3, s3_len);
    dst += s3_len;
    start_substr = end_substr + s2_len;
  }

  /* copy remainder of s1, including trailing '\0' */
  size_t remains = s1_len - (start_substr - s1) + 1;
  assert(dst + remains == newstr + newstr_len + 1);
  memcpy(dst, start_substr, remains);
  assert(strlen(newstr) == newstr_len);
  return newstr;
}

static char* format_fortran_float(char *result, double   number) {
  // First, we'll learn the exponent and adjust the number to the range [1.0,0.0]
  int exponent = 0;
  int signal = 0;
  if (number < 0) {
    number = -number;
    signal = 1;
    if (number == 1) {
      number /= 10;
      exponent++;
    }
  }
  for (; number > 1.0; exponent++) {
    number /= 10;
  }
  for (; (number < 0.1) && (number !=0) ; exponent--) {
    number *= 10;
  }

  // Next, we'll print the number as mantissa in [1,0] and exponent
  sprintf(result, "%0.4fE%+03d", number, exponent);
  if (signal) {
    result = strrep(result, "0.", "-.");
  }
  // Finally, we'll return the new string
  return result;
}

static int fill_null_linear_material(FILE *fp) {
  int i,j;
  char buf[15];
  for (i = 0; i < 10; i++) {
      for (j = 0; j < 10; j++) {
	fprintf(fp, "%s", format_fortran_float(&buf[0], 0.0));
      }
      fprintf(fp, "\n");
    }
  return 0;
}

static int fill_null_material(FILE *fp, int from) {
  char buf[15];
  int i, j;
  for (i = from; i < LIMIT_MATERIAL; i++) {
    if (i < 10) {
      fprintf(fp, "  %d                     \n", i);
    }
    else {
      fprintf(fp, " %d                     \n", i);
    }
    fprintf(fp, "NAO%s", format_fortran_float(&buf[0], 0.0));
    for (j = 0; j < FIRST_LINE_COLUMNS; j++) {
      fprintf(fp, "%s", format_fortran_float(&buf[0], 0.0));
    }
    fprintf(fp, "\n");

    fill_null_linear_material(fp);
  }
  return 0;
}

static int fill_material(FILE *fp, pso_particle_t p, cfg_t cfg, int from) {
  char buf[15];
  double bx, by, angle;
  int i, j;

  for (i = from; i < from + cfg.n_var; i++) {
    if (i < 10) {
      fprintf(fp, "  %d ima%d                \n", i, i);
    }
    else {
      fprintf(fp, " %d ima%d               \n", i, i);
    }
    fprintf(fp, "IMA%s", format_fortran_float(&buf[0], 1.0));
    angle = p.x[i-from];
    bx = cfg.bmax * cos(angle);
    fprintf(fp, "%s",format_fortran_float(&buf[0], bx));

    by = cfg.bmax * sin(angle);
    fprintf(fp, "%s",format_fortran_float(&buf[0], by));
    
    for (j = 2; j < FIRST_LINE_COLUMNS; j++) {
      fprintf(fp, "%s", format_fortran_float(&buf[0], 0.0));
    }
    fprintf(fp, "\n");

    fill_null_linear_material(fp);
  }
  return 0;
}

static int create_input_efcs(pso_particle_t p, cfg_t cfg, int particle) {
  FILE *fp;
  char buf[100];
  sprintf(&buf[0], "%03d/%s", particle, cfg.input_efcs);
  fp = fopen(buf, "w");
  if (fp == NULL) {
    printf("Erro de gravacao de arquivo\n");
    return -1;
  }
  fprintf(fp, "%s\n", cfg.sim_name);
  fprintf(fp, "\n"); //mesh plotting?
  fprintf(fp, "\n"); //Cartesian or axissimetrica
  fprintf(fp, "\n"); //Linear or non linear				     
  fclose(fp);
  
  return 0;
}

static int create_input_efcsouts(pso_particle_t p, cfg_t cfg, int particle) {
  FILE *fp;
  char buf[100];
  char out_path[100];
  sprintf(&buf[0], "%03d/%s", particle, cfg.input_efcsouts);
  fp = fopen(buf, "w");
  if (fp == NULL) {
    printf("Erro de gravacao de arquivo\n");
    return -1;
  }
  fprintf(fp, "%s\n", cfg.sim_name);
  fprintf(fp, "\n"); //mesh plotting
  sprintf(&out_path[0], "%03d.out", particle);
  fprintf(fp, "%s\n", out_path); //Linear or non linear				     
  fclose(fp);
  
  return 0;
}

static int exec_special_cmd_parallel(char *cmd, int particle) {
  char buf[100];
  pid_t child_pid, end_pid;
  int t, status;
  
  //child_pid = fork();
  //if (child_pid == 0) {
    sprintf(&buf[0], "%03d", particle);
    if (chdir(buf) < 0) {
      printf("error running changing dir\n");
    }


    if (system(cmd) < 0) {
      printf("error running %s\n", cmd);
    }

    if (chdir("../") < 0) {
      printf("error running changing dir\n");
    }
    
    //  _exit(0);
    //}
    //else {
    //for (t = 0; t < PID_TIMEOUT; t++) {
    // usleep(1000); // wait 1ms
    // end_pid = waitpid(child_pid, &status, WNOHANG|WUNTRACED);
      
    //if (end_pid == child_pid) {
	// child is finished
    //	break;
    // }
    //}
    //}
  return 0;
}

/******************************************************
 *
 *
 * external functions
 *
 *
******************************************************/
int aux_exec_script(pso_particle_t p, cfg_t cfg, int particle) {
  char cmd[100];
  pid_t child_pid, end_pid;
  int t, status;

  /*if (create_input_efcs(p, cfg, particle) < 0) {
    return -1;
  }

  if (create_input_efcsouts(p, cfg, particle) < 0) {
    return -1;
    }*/
  
  sprintf(&cmd[0], "./script%03d.sh", particle);
  
  child_pid = fork();
  if (child_pid == 0) {

    if (system(cmd) < 0) {
      printf("error running %s\n", cmd);
      return -1;
    }
   
    _exit(0);
  }
  else {
    for (t = 0; t < PID_TIMEOUT; t++) {
      usleep(1000); // wait 1ms
      end_pid = waitpid(child_pid, &status, WNOHANG|WUNTRACED);
      
      if (end_pid == child_pid) {
	// child is finished
    	break;
      }
    }
  }
  return 0;
}

int aux_gera_efmat(pso_particle_t p, cfg_t cfg, int particle) {
  FILE *fp;
  char buf[15], path[100];
  int i;
  
  sprintf(&path[0],"%03d/efmat.dat", particle);
  fp = fopen(path, "w");
  if (fp == NULL) {
    printf("didn't find efmat.dat path to create.\n");
    return -1;
  }

  // write air material
  fprintf(fp, "  1 ar                  \n");
  fprintf(fp, "LIN%s", format_fortran_float(&buf[0], 1.0));
  for (i = 0; i < FIRST_LINE_COLUMNS; i++) {
    fprintf(fp, "%s", format_fortran_float(&buf[0], 0.0));
  }
  fprintf(fp, "\n");

  fill_null_linear_material(fp);

  if (fill_material(fp, p, cfg, 2) < 0) {
    return -1;
  }
  
  if (fill_null_material(fp, 2 + cfg.n_var) < 0) {
    return -1;
  }
  fclose(fp);
  return 0;
}

int aux_exec_efcs(pso_particle_t p, cfg_t cfg, int particle) {
  char cmd_efcs[100];
  
  /*if (create_input_efcs(p, cfg, particle) < 0) {
    return -1;
    }*/

  sprintf(&cmd_efcs[0], "%s < %s > /dev/null", EFCS_PATH, cfg.input_efcs);
  if (exec_special_cmd_parallel(cmd_efcs, particle) < 0) {
    printf("error running %s\n", cmd_efcs);
  }
  return 0;
}

int aux_exec_efcsouts(pso_particle_t p, cfg_t cfg, int particle) {
  char cmd_efcsouts[100];
  
  /*if (create_input_efcsouts(p, cfg, particle) < 0) {
    return -1;
    }*/

  sprintf(&cmd_efcsouts[0], "%s < %s > /dev/null", EFCSOUTS_PATH, cfg.input_efcsouts);
  if (exec_special_cmd_parallel(cmd_efcsouts, particle) < 0) {
    printf("error running %s\n", cmd_efcsouts);
  }
  return 0;
}
